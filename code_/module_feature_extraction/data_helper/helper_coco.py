from pycocotools.coco import COCO
from enum import IntEnum
from .helper import *
import math

Tag_List_Path = './dataset/coco/tag_list/'
Image_Path = './dataset/coco/images/'
annotation_Path = './dataset/coco/annotations/'

Number_Of_Images_Train = 118287

class DataSetType (IntEnum):
    Train17 = 0
    Test17 = 1
    Valid17 = 2

class coco_helper (dataset_helper):

    def __init__(self, data_set_type, name="", min_tag_num=1, filter_rows=None, coco=None):
        super().__init__() 

        if data_set_type == DataSetType.Train17:
            self.dataType='train2017'
        elif data_set_type == DataSetType.Valid17:
            self.dataType='val2017'
        elif data_set_type == DataSetType.Test17:
            self.dataType='test2017'
        else:
            print("coco type is wrong")
            return
        if len(name) > 0:
            self.name = name 
        else:
            self.name = self.dataType

        if coco == None:
            annFile = annotation_Path + 'instances_{}.json'.format(self.dataType)
            self.coco=COCO(annFile)
        else:
            self.coco=coco

        cats = self.coco.loadCats(self.coco.getCatIds())
        self.tag_list = [cat['name'] for cat in cats]
        self.tag_id_list = [cat['id'] for cat in cats]
        self.tag_num = len(self.tag_list)

        self.image_ids_list = self.coco.getImgIds()
        self.image_ids_list.sort()
        self.image_number = len(self.image_ids_list)
        self.image_ids_dic = dict(zip(self.image_ids_list, range(0,self.image_number)))

        self.image_path_list = []
        for index in range(self.image_number):
            img = self.coco.loadImgs(self.image_ids_list[index])[0]
            self.image_path_list.append(Image_Path + '%s/%s'%(self.dataType,img['file_name']))

        # compute tag set for each image
        self.image_tags = [[0] * self.tag_num for i in range(self.image_number)]
        self.image_number_for_tag = []
        #self.image_ids_for_tag = []
        self.is_tail = [0] * self.tag_num
        for i, catId in enumerate(self.tag_id_list):
            imgIds = self.coco.getImgIds(catIds=catId)
            self.image_number_for_tag.append(len(imgIds))
            #self.image_ids_for_tag.append(imgIds)
            if self.image_number_for_tag[i] < self.image_number / 100 * 2:
                self.is_tail[i] = 1
            for id in imgIds:
                self.image_tags[self.image_ids_dic[id]][i] = 1

        if filter_rows != None:
            self.filter_data(filter_rows)

        if self.none_tag:
            self.add_none_tag()
        else:
        # just save the images with at least "min_tag_num" tags
            tag_num_per_image = [sum(self.image_tags[i]) for i in range(len(self.image_tags))]
            rows = [i for i in range(len(tag_num_per_image)) if tag_num_per_image[i] >= min_tag_num]
            self.filter_data(rows)

        self.tag_weight = [math.ceil(math.log(num, 2)) for num in self.image_number_for_tag]

        sum_weights = sum(self.tag_weight)
        self.tag_weight = [weight / sum_weights for weight in self.tag_weight]

        sum_image_number_for_tag = sum(self.image_number_for_tag)
        self.tag_weight1 = [num / sum_image_number_for_tag for num in self.image_number_for_tag]
        
        self.image_tags = np.asarray(self.image_tags)

         # compute image set for each tag
        self.tag_images = []
        print('image_number:',self.image_number)
        for i in range(len(self.tag_list)):
            temp = [j for j in range(len(self.image_tags)) if self.image_tags[j][i] == 1]
            self.tag_images.append(temp)
        tag_num_per_image = [sum(self.image_tags[i]) for i in range(len(self.image_tags))]
        self.mean_tag_num = sum(tag_num_per_image)/len(tag_num_per_image)
        print("mean_tag_num",self.mean_tag_num)

    def divide_into_train_valid_test(data_set_type, train_p=0.8, min_tag_num=1, test=False, seed=42):
        if data_set_type == DataSetType.Train17:
            dataType='train2017'
        elif data_set_type == DataSetType.Valid17:
            dataType='val2017'
        elif data_set_type == DataSetType.Test17:
            dataType='test2017'
        else:
            print("coco type is wrong")
            return
        annFile = annotation_Path + 'instances_{}.json'.format(dataType)
        coco=COCO(annFile)
        image_ids_list = coco.getImgIds()
        #image_ids_list.sort()  
        random.seed(seed) 
        valid_test_rows = random.sample(range(len(image_ids_list)), int((1-train_p) * len(image_ids_list)))
        test_rows = valid_test_rows[0:int(len(valid_test_rows)/2)]
        valid_rows = valid_test_rows[int(len(valid_test_rows)/2):]
        valid_test_rows.sort()
        train_rows = []
        pos = 0
        for i in range(len(image_ids_list)):
            if pos >= len(valid_test_rows) or i != valid_test_rows[pos]:
                train_rows.append(i)
            else:
                pos = pos + 1
        print("total_len: ", len(image_ids_list),",  train_len: ", len(train_rows),",  valid_len: ", len(valid_rows),",  test_len: ", len(test_rows))
        if test:
            train_data = coco_helper(data_set_type=data_set_type, min_tag_num=min_tag_num, 
                                    filter_rows=train_rows[0:3000], name="training", coco=coco)
            valid_data = coco_helper(data_set_type=data_set_type, min_tag_num=min_tag_num, 
                                    filter_rows=valid_rows[0:3000], name="validation", coco=coco)
            test_data = coco_helper(data_set_type=data_set_type, min_tag_num=min_tag_num, 
                                    filter_rows=test_rows[0:3000], name="testing", coco=coco)
            return train_data, valid_data, test_data

        train_data = coco_helper(data_set_type=data_set_type, name="training", min_tag_num=min_tag_num, 
                                filter_rows=train_rows,coco=coco)
        valid_data = coco_helper(data_set_type=data_set_type, name="validation", min_tag_num=min_tag_num, 
                                filter_rows=valid_rows, coco=coco)
        test_data  = coco_helper(data_set_type=data_set_type, name="testing", min_tag_num=min_tag_num, 
                                filter_rows=test_rows, coco=coco)
        return train_data, valid_data, test_data

    def filter_data(self, rows):
        super().filter_data(rows)
        self.image_ids_list = [self.image_ids_list[i] for i in rows]
        self.image_ids_dic = dict(zip(self.image_ids_list, range(0,self.image_number)))
        
    def refresh_images(self):
        for index in range(self.image_number):
            img = self.coco.loadImgs(self.image_ids_list[index])[0]
            img_path = self.Image_Path + '%s/%s'%(self.dataType,img['file_name'])
            img = cv2.imread(img_path)
            scale = min(img.shape[0]/IMAGE_HEIGHT, img.shape[1]/IMAGE_WIDTH) -0.1
            print(img.shape)
            if scale < 1.05:
                continue
            img = cv2.resize(img, (int(img.shape[1] / scale), int(img.shape[0] / scale)))
            cv2.imwrite(img_path, img)
            print(img.shape)
            print(index, scale)