import torch
from .helper import *
import pandas as pd
import math

Image_Path = './dataset/artigo/images/'
dataset_Path = './dataset/artigo/'
Image_Tags_Path = './dataset/artigo/image_tags.csv'

class artigo_helper (dataset_helper):

    def __init__(self, name, min_tag_num=1, filter_rows=None):
        super().__init__() 
        self.name = name 

        try:
            self.cleaned_image_info_list = pd.read_csv(dataset_Path + "cleaned_resource.csv")
            self.image_tags = pd.read_csv(dataset_Path + "cleaned_image_tags.csv")
            self.tag_list = pd.read_csv(dataset_Path + "cleaned_tag_list.csv")
        except IOError:
            # compute tag_list
            tag_list = pd.read_csv(dataset_Path + "tag.csv")
            tag_id_dict = {}
            tag_id_list = []
            for index,row in tag_list.iterrows():
                tag_id_dict[row[0]] = index
                tag_id_list.append(row[0])

            #relation_list = pd.read_csv(dataset_Path + "relation.csv")
            image_list = pd.read_csv(dataset_Path + "resource.csv")
            tagging_list = pd.read_csv(dataset_Path + "tagging.csv")
            tagging_at_least_3 = tagging_list.drop( tagging_list[tagging_list["frequency"] <= 3].index)
            cleared_tagging = tagging_at_least_3
            tag_accurency = [0] * len(tag_list)
            for index, row in cleared_tagging.iterrows():
                tag_accurency[tag_id_dict[row[2]]] = tag_accurency[tag_id_dict[row[2]]] + 1
            tag_1000_rows = [i for i in range(len(tag_list)) if tag_accurency[i] >= 300]
            
            self.tag_num = len(tag_1000_rows)
            self.tag_list = [tag_list["name"][index] for index in tag_1000_rows]              
            self.tag_id_list = [tag_list["id"][index] for index in tag_1000_rows]
            self.tag_ids_dic = dict(zip(self.tag_id_list, range(0,self.tag_num)))


            self.image_number = len(image_list)
            self.image_ids_list = [image_list["id"][index] for index in range(self.image_number)]
            self.image_ids_dic = dict(zip(self.image_ids_list, range(0,self.image_number)))
            self.image_path_list = [Image_Path + image_list["path"][index] for index in range(self.image_number)]

            # compute tag set for each image
            self.image_tags = [[0] * self.tag_num for i in range(self.image_number)]
            self.image_number_for_tag = []
            
            tag_id_set = set(self.tag_id_list)
            for index, tagging in tagging_at_least_3.iterrows():
                if tagging["tag_id"] in tag_id_set:
                    self.image_tags[self.image_ids_dic[tagging["resource_id"]]][self.tag_ids_dic[tagging["tag_id"]]] = 1
            
            self.image_number_for_tag = torch.sum(torch.tensor(self.image_tags).to(device).t(), dim=-1).tolist() 
           
            if filter_rows != None:
                self.filter_data(filter_rows)

            if self.none_tag:
                self.add_none_tag()
            else:
                # just save the images with at least "min_tag_num" tags
                tag_num_per_image = [sum(self.image_tags[i]) for i in range(len(self.image_tags))]
                rows = [i for i in range(len(tag_num_per_image)) if tag_num_per_image[i] >= min_tag_num]
                self.filter_data(rows)
            self.image_tags = np.asarray(self.image_tags)

            self.tag_weight = [ math.ceil(math.log(num, 2)) for num in self.image_number_for_tag]
            sum_weights = sum(self.tag_weight)
            self.tag_weight = [weight / sum_weights for weight in self.tag_weight]

            # compute image set for each tag
            self.tag_images = []
            print('image_number:',self.image_number)
            for i in range(len(self.tag_list)):
                temp = [j for j in range(len(self.image_tags)) if self.image_tags[j][i] == 1]
                self.tag_images.append(temp)
            tag_num_per_image = [sum(self.image_tags[i]) for i in range(len(self.image_tags))]
            self.mean_tag_num = sum(tag_num_per_image)/len(tag_num_per_image)
            print("mean_tag_num:",self.mean_tag_num)

    def divide_into_train_valid_test(train_p=0.8, min_tag_num=1, test=False, seed=42):

        try:
            image_info_list = pd.read_csv(dataset_Path + "resource.csv")
        except IOError:
            print("no images info file")
            return

        random.seed(seed)
        valid_test_rows = random.sample(range(len(image_info_list)), int((1-train_p) * len(image_info_list)))
        valid_rows = valid_test_rows[0:int(len(valid_test_rows)/2)]
        test_rows = valid_test_rows[int(len(valid_test_rows)/2):]
        valid_test_rows.sort()
        train_rows = []
        pos = 0
        for i in range(len(image_info_list)):
            if pos >= len(valid_test_rows) or i != valid_test_rows[pos]:
                train_rows.append(i)
            else:
                pos = pos + 1
        print("total_len: ", len(image_info_list),",  train_len: ", len(train_rows),",  valid_len: ", len(valid_rows),",  test_len: ", len(test_rows))
        if test:
            train_data = artigo_helper("training", min_tag_num=min_tag_num, filter_rows=train_rows[0:1000])
            valid_data = artigo_helper("validation", min_tag_num=min_tag_num, filter_rows=valid_rows[0:1000])
            test_data = artigo_helper("testing", min_tag_num=min_tag_num, filter_rows=test_rows[0:1000])
            return train_data, valid_data, test_data
        train_data = artigo_helper("training", min_tag_num=min_tag_num, filter_rows=train_rows)
        valid_data = artigo_helper("validation", min_tag_num=min_tag_num, filter_rows=valid_rows)
        test_data = artigo_helper("testing", min_tag_num=min_tag_num, filter_rows=test_rows)
        return train_data, valid_data, test_data

    def filter_data(self, rows):
        super().filter_data(rows)
        self.image_ids_list = [self.image_ids_list[i] for i in rows]
        self.image_ids_dic = dict(zip(self.image_ids_list, range(0,self.image_number)))



