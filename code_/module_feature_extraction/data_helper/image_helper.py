import torch
from torchvision import transforms
import os
from PIL import Image
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True

if torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')


IMAGE_HEIGHT = 224
IMAGE_WIDTH = 224
IMAGE_CHANNEL = 3

mySeed = 42

class image_helper (torch.utils.data.Dataset):

    def __init__(self, path='/clusterarchive/MET_data/imagesSmall'):
        super().__init__() 
        self.path = path
        self.image_path_list = os.listdir(path)
        self.image_number = len(self.image_path_list)
    
    
    def get_image(self, index):
        return self.get_image_by_path(self.get_image_path_by_index(index))

    def get_image_by_path(self, image_path):
        
        image = Image.open(image_path)
        if image.mode != "RGB":
            image = image.convert('RGB')
        transform = transforms.Compose([transforms.Resize((IMAGE_WIDTH, IMAGE_HEIGHT)),
                                    transforms.ToTensor()])
        image = transform(image)
        return image

    def get_images(self, indexes):
        res = None
        for index in indexes:
            temp = self.get_image(index)
            temp = temp.view((1,) + temp.shape)
            if res == None:
                res = temp
            else:
                res = torch.cat((res, temp), 0)
        return res

    def get_tag_matrix(self, indexes):
        res = None
        for index in indexes:
            temp = self.get_tags(index)
            temp = temp.view((1,) + temp.shape)
            if res == None:
                res = temp
            else:
                res = torch.cat((res, temp), 0)
        return res

    def get_tags(self, index):
        return torch.tensor([0])

    def __getitem__(self, index):
        return self.get_image(index), self.get_tags(index)

    def __len__(self):
        return self.image_number

    def get_image_path_by_index(self, index):
        return self.path + '/' + self.image_path_list[index]

    


