import torch
from torchvision import transforms
import numpy as np
import cv2

from .data_augmentation import *

if torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')


IMAGE_HEIGHT = 224
IMAGE_WIDTH = 224
IMAGE_CHANNEL = 3


class dataset_helper (torch.utils.data.Dataset):

    def __init__(self):
        super().__init__() 
        self.data_augmentation = True
        self.none_tag = False

    def turn_off_data_augmentation(self):
        self.data_augmentation = False

    def turn_on_data_augmentation(self):
        self.data_augmentation = True

    def add_none_tag(self):
        self.tag_list.append('none')
        self.tag_num = len(self.tag_list)
        no_tag = [int(sum(self.image_tags[i]) == 0) for i in range(len(self.image_tags))]
        #print('sum of tag_or_none', sum(no_tag))
        self.image_number_for_tag.append(sum(no_tag))
        #print(no_tag[0])
        #print(self.image_tags[0])
        self.image_tags = [self.image_tags[i] + [no_tag[i]] for i in range(self.image_number)]
        #print(self.image_tags[0])


    def reorder(self, data, order):
        return [data[i] for i in order]

    def filter_data(self, rows):
        self.image_tags = [self.image_tags[i] for i in rows]
        self.image_number = len(self.image_tags)
        self.image_path_list = [self.image_path_list[i] for i in rows]

    def get_weight(self, tag_matrix):
        res = []
        for tag_v in tag_matrix:
            res.append(sum(tag_v * self.tag_weight).item())
        return torch.from_numpy(np.asarray(res)).to(device)

    def get_tag_list(self):
        return self.tag_list

    def get_image(self, index):
        return self.get_image_by_path(self.image_path_list[index])

    def get_image_by_path(self, image_path):
        
        image = Image.open(image_path)
        if image.mode != "RGB":
            image = image.convert('RGB')
        if self.data_augmentation:
            image = data_augment(image)
        transform = transforms.Compose([transforms.Resize((IMAGE_WIDTH, IMAGE_HEIGHT)),
                                    transforms.ToTensor()])
        image = transform(image)
        return image

    def get_images(self, indexes):
        res = []
        for index in indexes:
            temp = self.get_image(index)
            temp = temp.view((1,) + temp.shape)
        res = torch.cat(res)
        return res

    def get_tag_matrix(self, indexes):
        res = None
        for index in indexes:
            temp = self.get_tags(index)
            temp = temp.view((1,) + temp.shape)
            if res == None:
                res = temp
            else:
                res = torch.cat((res, temp), 0)
        return res

    def get_tags(self, index):
        return torch.from_numpy(self.image_tags[index])

    def __getitem__(self, index):
        return self.get_image(index), self.get_tags(index)

    def __len__(self):
        return self.image_number

    def get_image_path_by_index(self, index):
        return self.image_path_list[index]

    def get_image_path_by_name(self, name):
        return self.Image_Path + '%s/%s'%(self.dataType, name)

    # get an image list and tag matrix based on a single tag list
    def get_samples(self, num, based_on_weight=True):
        with torch.no_grad():
            if based_on_weight:
                tag_weight = self.tag_weight
            else:
                tag_weight = [1.0/self.tag_num] * self.tag_num
            sampled_tags = np.random.choice(self.tag_num, num, p=tag_weight)
            sample_num_for_tag = []
            for i in range(self.tag_num):
                sample_num_for_tag.append(np.sum(sampled_tags == i))
            index_list = []
            for index in range(self.tag_num):
                sample_num = sample_num_for_tag[index]
                image_indices = random.sample(self.tag_images[index], min(sample_num,len(self.tag_images[index])))
                index_list = index_list + image_indices
            tag_matrix = self.get_tag_matrix(index_list).to(device)
        return tag_matrix

        if get_images:
            images = self.get_images(index_list).to(device)
            return images, tag_matrix

    # get an image list and tag matrix based on a single tag list
    def get_n_tag_vector_samples(self, num):
        single_tag_pool = []
        for tag in range(self.tag_num):
            single_tag_pool = single_tag_pool + [tag] * self.tag_weight[tag]
        while len(single_tag_pool) < num:
            single_tag_pool = single_tag_pool + single_tag_pool
        sampled_single_tag_list = random.sample(single_tag_pool, num)
        index_list = []
        for tag_index in sampled_single_tag_list:
            image_index = random.sample(self.tag_images[tag_index], 1)[0]
            index_list.append(image_index)
        tag_matrix = self.get_tag_matrix(index_list).to(device)
        return tag_matrix

    def print_tag_frequence(self):
        tag_freqence_list = []
        for i in range(len(self.tag_list)):
            tag = self.tag_list[i]
            frequence = self.image_number_for_tag[i]
            tag_freqence_list.append(tag + ':' + str(frequence))

        return tag_freqence_list

    def get_tag_matrix_based_on_tagset(self, tag_name_set):
        tag_matrix = torch.zeros((1, self.tag_num))
        tag_name_index_dic = dict(zip(self.tag_list, range(0,self.tag_num)))
        for tag in tag_name_set:
            tag_matrix[0,tag_name_index_dic[tag]] = 1
        return tag_matrix