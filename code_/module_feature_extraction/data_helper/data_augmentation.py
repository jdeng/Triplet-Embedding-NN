
from curses import newwin
import torch
from torchvision import transforms
import PIL 
import IPython.core.display as display
from PIL import ImageEnhance, Image
import random

def data_augment(image):
    a = random.randint(0, 10000)
    if a % 2 == 0:
        image = image.transpose(Image.FLIP_LEFT_RIGHT)
    if a % 53 == 0:
        image = image.convert("L").convert('RGB')
    if a % 11 == 0:
        w, h = image.size
        d = abs(w-h)
        if w > h:
            new_w = h + random.randint(int(d/2), d)
            new_h = h
        elif w < h:
            new_w = w
            new_h = w + random.randint(int(d/2), d)
        else:
            d = random.randint(0, int(w/10))
            new_w = w - d
            new_h = h - d 
        image = image.crop(((w-new_w) >> 1, (h-new_h) >> 1, (w+new_w) >> 1, (h+new_h) >> 1))
    image = ImageEnhance.Brightness(image).enhance(random.gauss(1, 0.1))
    image = ImageEnhance.Color(image).enhance(random.gauss(1, 0.1))
    image = ImageEnhance.Sharpness(image).enhance(random.gauss(1, 0.1))

    return image

def test_data_augmentation():
    image_path = './dataset/coco/images/val2017/000000213816.jpg'
    image = PIL.Image.open(image_path)
    display.display(image)
    aug_image = data_augment(image)
    display.display(aug_image)
