def get_tag_set_str(tag_set):
    s = ""
    for index in range(len(tag_set)):
        if tag_set[index] == 1:
            s = s + str(index) + "-"
    return s


def count_tag_set(data):
    dict = {}
    for tag_set in data.image_tags:
        if get_tag_set_str(tag_set) not in dict:
            dict[get_tag_set_str(tag_set)] = 1
    print("There are " + str(len(dict)) + " tag sets.")
    return len(dict)

def compute_upper_bound_micro_evaluation(data, k):
    image_num_list = statistics_tag_num(data)
    tp = 0
    sum_p = 0
    sum_g = 0
    for index in range(len(image_num_list)):
        if image_num_list[index] != 0:
            sum_g += image_num_list[index] * index
            if k < 0:
                sum_p += image_num_list[index] * index
                tp += image_num_list[index] * index
            else:
                sum_p += image_num_list[index] * k
                tp += image_num_list[index] * min(index, k)
            
    precision = tp / sum_p
    recall = tp / sum_g
    if precision + recall == 0:
        F1 = 0
    else:
        F1 = 2 * (precision * recall) / (precision + recall)
    if k > 0:
        print(f"upper bound with {k} closest tags:")
    else:
        print("upper bound with undetermined number of the closest tags:")
    print("precision:", precision, "    recall", recall, "    F1:", F1)
    return 


def statistics_tag_num(data):
    # index is the tag number
    image_num_list = [0] * len(data.image_tags[0])
    for tag_set in data.image_tags:
        image_num_list[sum(tag_set)] += 1
    res = []
    for index in range(len(image_num_list)):
        if image_num_list[index] != 0:
            res.append(str(index) + ":" + str(image_num_list[index]))
    print("Tag number statistics:")
    print(res)
    return image_num_list

def analysiz_data(data, k):
    count_tag_set(data)
    compute_upper_bound_micro_evaluation(data, k)
    print("image number:", data.image_number)
    print("tag number:", data.tag_num)
    print("tag:")
    print(data.tag_list)
    print("image number per tag:")
    print(data.image_number_for_tag)
    #print("weights per tag:")
    #print(data.tag_weight)


