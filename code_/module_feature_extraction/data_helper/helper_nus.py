import numpy as np
import torch
import random

from enum import IntEnum
from .helper import *

Image_List_Path_Train = './dataset/nus_wide/ImageList/TrainImageList.txt'
Image_List_Path_Test  = './dataset/nus_wide/ImageList/TestImageList.txt'
Image_List_Path_All   = './dataset/nus_wide/ImageList/ImageList.txt'

Image_Tags_Path_Train_1k = './dataset/nus_wide/NUS_WID_Tags/TrainTags1k.txt'
Image_Tags_Path_Test_1k  = './dataset/nus_wide/NUS_WID_Tags/TestTags1k.txt'
Image_Tags_Path_All_1k   = './dataset/nus_wide/NUS_WID_Tags/AllTags1k.txt'

Image_Tags_Path_Train_81 = './dataset/nus_wide/NUS_WID_Tags/TrainTags81.txt'
Image_Tags_Path_Test_81  = './dataset/nus_wide/NUS_WID_Tags/TestTags81.txt'
Image_Tags_Path_All_81   = './dataset/nus_wide/NUS_WID_Tags/AllTags81.txt'

Tag_List_Path_81 = './dataset/nus_wide/ConceptsList/TagList81.txt'
Tag_List_Path_1k = './dataset/nus_wide/ConceptsList/TagList1k.txt'
Tag_List_Path_All = './dataset/nus_wide/ConceptsList/TagListAll.txt'

Image_Path = './dataset/nus_wide/Flickr/'
Image_URLs_Path = './dataset/nus_wide/NUS-WIDE-urls.txt'

Number_Of_Images_All = 269648
Number_Of_Images_Train = 161789
Number_Of_Images_Test = 107859
Number_Of_Images_Valid = 10000


class nus_helper (dataset_helper):
    
    #'All_All_tags' No complete data set
    class DataSetType (IntEnum):
        Train_81 = 0
        Test_81 = 1
        All_81 = 2
        Train_1k = 3 
        Test_1k = 4 
        All_1k = 5
        
    def __init__(self, data_set_type, name, min_tag_num=1, filter_rows=None):
        super().__init__() 
        self.name = name

        if (data_set_type % 3) == 0:
            self.image_list_path = Image_List_Path_Train
            #self.image_number = Number_Of_Images_Train
        elif (data_set_type % 3) == 1:
            self.image_list_path = Image_List_Path_Test
            #self.image_number = Number_Of_Images_Test
        else:
            self.image_list_path = Image_List_Path_All
            #self.image_number = Number_Of_Images_All

        if data_set_type < 3:
            self.tag_list_path = Tag_List_Path_81
        else:
            self.tag_list_path = Tag_List_Path_1k

        if data_set_type == self.DataSetType.Train_81:
            self.image_tags_path = Image_Tags_Path_Train_81
        elif data_set_type == self.DataSetType.Test_81:
            self.image_tags_path = Image_Tags_Path_Test_81
        elif data_set_type == self.DataSetType.All_81:
            self.image_tags_path = Image_Tags_Path_All_81
        elif data_set_type == self.DataSetType.Train_1k:
            self.image_tags_path = Image_Tags_Path_Train_1k
        elif data_set_type == self.DataSetType.Test_1k:
            self.image_tags_path = Image_Tags_Path_Test_1k
        else:
            self.image_tags_path = Image_Tags_Path_All_1k

        self.image_path = Image_Path
        self.image_urls_path = Image_URLs_Path

        self.tag_list = [line.strip() for line in open(self.tag_list_path).readlines()]
        self.tag_num = len(self.tag_list)
        
        # read images
        image_list = np.loadtxt(self.image_list_path, dtype=str)
        
        self.image_number = len(image_list)
        self.image_path_list = [Image_Path + image_list[index].replace("\\","/") for index in range(self.image_number)]

        #read tags
        self.image_tags = np.multiply(np.loadtxt(self.image_tags_path, dtype=bool), 1)
        self.image_number_for_tag = torch.sum(torch.tensor(self.image_tags).to(device).t(), dim=-1).tolist()

        self.tag_weight = [min(int(num/200 + 3), int(self.image_number /10 / 200)) 
                            for num in self.image_number_for_tag]

        if filter_rows != None:
            self.filter_data(filter_rows)

        # delete images, which cannot be opened
        rows = []
        for i in range(self.image_number):
            tn = int(np.sum(self.image_tags[i]))
            if tn >= min_tag_num:
                try:
                    Image.open(self.image_path_list[i]).size
                    #cv2.imread(self.image_path_list[i]).shape
                except:
                    print("cannot open ", self.image_path_list[i])
                    continue
                rows.append(i)
        self.filter_data(rows)
        
        # just save the images with at least "min_tag_num" tags
        tag_num_per_image = [sum(self.image_tags[i]) for i in range(len(self.image_tags))]
        rows = [i for i in range(len(tag_num_per_image)) if tag_num_per_image[i] >= min_tag_num]
        self.filter_data(rows)
        self.image_tags = np.asarray(self.image_tags)

         # compute image set for each tag
        self.tag_images = []
        print(len(self.image_tags),self.image_number)
        for i in range(len(self.tag_list)):
            temp = [j for j in range(len(self.image_tags)) if self.image_tags[j][i] == 1]
            self.tag_images.append(temp)
        tag_num_per_image = [sum(self.image_tags[i]) for i in range(len(self.image_tags))]
        self.mean_tag_num = sum(tag_num_per_image)/len(tag_num_per_image)
        print("self.mean_tag_num",self.mean_tag_num)

    def divide_into_test_valid(data_set_type, test_p=0.5, min_tag_num=1, test=False):
        if (data_set_type % 3) == 0:
            image_number = Number_Of_Images_Train
        elif (data_set_type % 3) == 1:
            image_number = Number_Of_Images_Test
        else:
            image_number = Number_Of_Images_All

        random.seed(mySeed)
        valid_rows = random.sample(range(image_number), int((1-test_p) * image_number))
        valid_rows.sort()
        train_rows = []
        pos = 0

        for i in range(image_number):
            if pos >= len(valid_rows) or i != valid_rows[pos]:
                train_rows.append(i)
            else:
                pos = pos + 1
        print("total_len: ", image_number,",  train_len: ", len(train_rows),",  valid_len: ", len(valid_rows))
        if test:
            test_data = nus_helper(data_set_type, "testing", min_tag_num=min_tag_num, filter_rows=train_rows[0:1000])
            valid_data = nus_helper(data_set_type, "validation", min_tag_num=min_tag_num, filter_rows=valid_rows[0:1000])
            return test_data, valid_data
        test_data = nus_helper(data_set_type, "testing", min_tag_num=min_tag_num, filter_rows=train_rows)
        valid_data = nus_helper(data_set_type, "validation", min_tag_num=min_tag_num, filter_rows=valid_rows)
        return test_data, valid_data

