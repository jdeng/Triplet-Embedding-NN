import torch.nn as nn
import torch.nn.functional as F
import torchvision.models as models


class Image_Model(nn.Module): # input batchSize * 1 * tagNum * tagNum 
    def __init__(self, type, freeze_layers=0):
        super().__init__()
        if type == "VGG16":
            self.model = models.vgg16(pretrained=True)
        elif type == "ResNet18":
            self.model = models.resnet18(pretrained=True)
        elif type == "ResNet34":
            self.model = models.resnet34(pretrained=True)
        elif type == "ResNet50":
            self.model = models.resnet50(pretrained=True)
        elif type == "ResNet101":
            self.model = models.resnet101(pretrained=True)
        elif type == "EfficientNet_b3":
            self.model = models.efficientnet_b3(pretrained=True)
        elif type == "EfficientNet_b4":
            self.model = models.efficientnet_b4(pretrained=True)
        elif type == "EfficientNet_b6":
            self.model = models.efficientnet_b6(pretrained=True)
        #print(self.model)
        i = 0
        for child in self.model.children():
            if i < freeze_layers:
                for param in child.parameters():
                    param.requires_grad = False
            i += 1
        
    def forward(self, x):
        out = self.model(x)
        return F.normalize(out)

