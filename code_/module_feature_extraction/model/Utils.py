import numpy as np
import torch
import random
import math
import GPUtil
 
if torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

Gpus = GPUtil.getGPUs()
max_F1 = 0
def get_similarity_matrix(tag_matrix_1, tag_matrix_2):
    t1 = tag_matrix_1.float()
    t2 = tag_matrix_2.float()
    return torch.mm(t1, t2.t()).int()

def compute_n_2_tags_matrix(tag_num, sample_num):
    num = min(sample_num, tag_num*tag_num)
    singe_tag_matrix = torch.eye(tag_num).to(device)
    singe_tag_matrix_1 = singe_tag_matrix.repeat(math.ceil(num/tag_num)*2,1)
    indices = random.sample(range(len(singe_tag_matrix_1)), len(singe_tag_matrix_1))
    singe_tag_matrix_2 = singe_tag_matrix_1[indices]
    two_tags_matrix = torch.abs(singe_tag_matrix_1 - singe_tag_matrix_2)
    indices = random.sample(range(len(singe_tag_matrix_1)), len(singe_tag_matrix_1))
    # not exictly sample_num
    return two_tags_matrix

def subset_extension(tag_matrix, options):
    max_length = options.tag_max_length
    tag_matrix_ex = [tag_matrix]
    tag_matrix = tag_matrix.repeat(max_length*4,1)

    for i in range(math.ceil(math.log(max_length, 2))):
        tag_num_list = torch.sum(tag_matrix, dim=1)
        indices = tag_num_list >= 2
        if len(indices) == 0:
            break
        tag_matrix = tag_matrix[indices]
        rand_01 = torch.randint(0, 2, tag_matrix.shape).to(device)
        tag_matrix = tag_matrix * rand_01
        tag_matrix_ex.append(tag_matrix)
    return torch.cat(tag_matrix_ex)


def all_tags_step_extension(tag_model, data, tag_matrix, num, options):
    with torch.no_grad():
        tn = len(tag_matrix[0])
        if options.sampling_from_dataset != 0:
            samples = data.get_samples(options.sampling_from_dataset, options.sampling_based_on_weight)
            tag_matrix_ex = torch.cat([tag_matrix] + [samples])
        else:
            tag_matrix_ex = tag_matrix
        if options.sampling_subset:
            tag_matrix_ex = subset_extension(tag_matrix_ex, options)
        if options.sampling_single_tag:
            tag_matrix_ex = torch.cat([tag_matrix] + [torch.eye(tn).to(device)])

        tag_num_list = torch.sum(tag_matrix_ex, dim=1)
        indices = (tag_num_list > 0) & (tag_num_list <= options.tag_max_length)
        tag_matrix_ex = tag_matrix_ex[indices]

        if options.sampling_2_tags:
            tag_matrix_ex = torch.cat([tag_matrix_ex, compute_n_2_tags_matrix(tn, max(0, num-len(tag_matrix_ex)))])

        tag_matrix_ex = torch.unique(tag_matrix_ex, dim=0)

        if len(tag_matrix_ex) > num:
            indices = random.sample(range(len(tag_matrix_ex)), num)
            tag_matrix_ex = tag_matrix_ex[indices]

    tag_features_ex = tag_model.compute_features(tag_matrix_ex)

    return tag_features_ex, tag_matrix_ex

def clip_tag_matrix(tag_matrix, max_length):
    with torch.no_grad():
        clipped_tag_matrix = tag_matrix.clone()
        tag_num = torch.sum(tag_matrix, dim=1)
        for i in range(len(tag_num)):
            if tag_num[i] > max_length:
                clipped_tag_matrix[i] = clip_tag_vector(clipped_tag_matrix[i], max_length)
    return clipped_tag_matrix

def clip_tag_vector(tag_vector, max_length):
    indices = (tag_vector == 1).nonzero()
    indices = indices[random.sample(range(len(indices)), max_length)]
    tag_vector.zero_()
    tag_vector[indices] = 1
    return tag_vector

def compute_cross_entropy_loss(options, data, loss_func, image_features, y_tag_matrix, tag_model):
    if len(options.batch_size) == 1:
        tag_features_ex = tag_model(y_tag_matrix)
        tag_matrix_ex = y_tag_matrix
    else:
        num = options.batch_size[-1]
        tag_features_ex, tag_matrix_ex = all_tags_step_extension(tag_model, data, y_tag_matrix, num, options)
    return loss_func(image_features, tag_features_ex, y_tag_matrix, tag_matrix_ex)

last_y_tag_matrix = None
last_x_images = None

def compute_step_metric_loss(options, data, loss_func, image_features, x_images, y_tag_matrix, tag_model, image_model):
    if len(options.batch_size) == 1:
        tag_features_ex = tag_model(y_tag_matrix)
        tag_matrix_ex = y_tag_matrix
    else:
        num = options.batch_size[-1]
        tag_features_ex, tag_matrix_ex = all_tags_step_extension(tag_model, data, y_tag_matrix, num, options)
    
    # TT loss
    ######################################################
    if False:
        batch_size = len(y_tag_matrix)
        indices = torch.tensor(random.sample(range(len(tag_matrix_ex)), int(batch_size/2))).to(device)
        clipped_y_tag_matrix = torch.cat([clip_tag_matrix(y_tag_matrix, options.tag_max_length), tag_matrix_ex[indices]])
        del indices
    else:
        clipped_y_tag_matrix = clip_tag_matrix(y_tag_matrix, options.tag_max_length)
    clipped_tag_features = tag_model(clipped_y_tag_matrix)
    TT_loss = loss_func(clipped_y_tag_matrix, tag_matrix_ex, clipped_tag_features, tag_features_ex, loss_type=3)
    del clipped_y_tag_matrix
    del clipped_tag_features

    # IT loss
    ######################################################
    IT_loss = loss_func(y_tag_matrix, tag_matrix_ex, image_features, tag_features_ex, loss_type=0)

    # TI loss
    ######################################################    
    global last_y_tag_matrix
    global last_x_images
    if False and last_x_images != None:
        batch_size = len(last_x_images)
        last_y_tag_matrix = last_y_tag_matrix[0:int(batch_size/4)]
        last_x_images = last_x_images[0:int(batch_size/4)]
        y_tag_matrix = torch.cat([last_y_tag_matrix, y_tag_matrix])
        x_images = torch.cat([last_x_images, x_images])
        image_features_last = image_model(last_x_images)
        image_features = torch.cat([image_features, image_features_last])
    if len(options.batch_size) != 2:
        tag_features_ex = tag_model(y_tag_matrix)
        tag_matrix_ex = y_tag_matrix
    TI_loss = loss_func(tag_matrix_ex, y_tag_matrix, tag_features_ex, image_features, loss_type=1)
    
    # II loss
    ######################################################
    II_loss = loss_func(y_tag_matrix, y_tag_matrix, image_features, image_features, loss_type=2)
    
    return IT_loss, TI_loss, II_loss, TT_loss

n = 0
def compute_loss(data, x_images, y_tag_matrix, image_model, tag_model, loss_func, options):

    image_features = image_model(x_images)
    IT_loss, TI_loss, II_loss, TT_loss = None, None, None, None

    if options.loss_function == "step" or options.loss_function == "triplet":
        IT_loss, TI_loss, II_loss, TT_loss = compute_step_metric_loss(options, data, loss_func, 
                                            image_features,  x_images, y_tag_matrix, tag_model, image_model)

        loss = options.loss_weight[0]*IT_loss[0] + options.loss_weight[1]*TI_loss[0] + options.loss_weight[2]*II_loss[0] + options.loss_weight[3]*TT_loss[0] 
        if n % 300 == 30:
            print(IT_loss[0], TI_loss[0], II_loss[0], TT_loss[0] )
    elif options.loss_function == "cross_entropy":
        loss = compute_cross_entropy_loss(options, data, loss_func, image_features, y_tag_matrix, tag_model)
           
    return loss, IT_loss, TI_loss, II_loss, TT_loss

def single_epoch_computation(image_model, tag_model, loader, loss_func, options, optim=1, updata=0):
    loss = 0
    compute_dis = False
    compute_0_loss = False    
    global last_y_tag_matrix
    global last_x_images
    global n 
    n = 0
    last_y_tag_matrix = None
    last_x_images = None
    if options.loss_function != "cross_entropy":
        compute_dis = True
        compute_0_loss = True
        compute_0_loss_per_tag = False
        # IT, TI, II, TT : pos., neg.
        pos_neg_dis = np.zeros((8,))
        tag_num = loader.dataset.tag_num
        # count the number of cases with 0 loss for each loss type and each tag
        proportion_0_loss_per_tag = np.zeros((4,tag_num))
        # count the number of cases with 0 loss for each loss type
        proportion_0_loss = np.zeros((4,))

    for (x_images,y_tag_matrix) in loader:
        n = n + 1
        x_images, y_tag_matrix = x_images.to(device), y_tag_matrix.to(device)    
        #loss, IT_loss, TI_loss, II_loss, TT_loss
        data = loader.dataset
        res = compute_loss(data, x_images, y_tag_matrix, image_model, tag_model, loss_func, options)
            
        last_y_tag_matrix = y_tag_matrix
        last_x_images = x_images

        if updata:
            optim[0].zero_grad()
            optim[1].zero_grad()
            res[0].backward()
            optim[0].step()
            optim[1].step()

        loss += res[0].item()

        if compute_dis:
            for i in range(8):
                pos_neg_dis[i] += res[1+int(i/2)][1+i%2]

        if compute_0_loss:
            for j in range(4):
                proportion_0_loss[j] += res[1+j][3]
                    
    loss /= len(loader)

    if options.loss_function == "cross_entropy":
        return loss,

    pos_neg_dis = pos_neg_dis / len(loader)

    if compute_0_loss_per_tag:
        print("The number of 0 loss per tag:")
        print(proportion_0_loss_per_tag)
        #print(loader.dataset.tag_list)
        #print(loader.dataset.image_number_for_tag)
        print()

    proportion_0_loss = proportion_0_loss / len(loader) 
    
    IT_res = [pos_neg_dis[0], pos_neg_dis[1], proportion_0_loss[0]]
    TI_res = [pos_neg_dis[2], pos_neg_dis[3], proportion_0_loss[1]]
    II_res = [pos_neg_dis[4], pos_neg_dis[5], proportion_0_loss[2]]
    TT_res = [pos_neg_dis[6], pos_neg_dis[7], proportion_0_loss[3]]

    return loss, IT_res, TI_res, II_res, TT_res 

