from random import random
from tqdm.notebook import tqdm
import IPython.core.display as display 

from .Utils import *


def output_loss_dis(loss_dis, options):
    print(f"    loss: {loss_dis[0]:.10f}" )
    if options.loss_function != "cross_entropy":
        print("    " +
                output_dis("IT", loss_dis[1]) +
                output_dis("TI", loss_dis[2]) +
                output_dis("II", loss_dis[3]) +
                output_dis("TT", loss_dis[4]) )
            
        print(f"    0_loss_IT: {loss_dis[1][2]:.2%},  ", 
                f"0_loss_TI: {loss_dis[2][2]:.2%},  ", 
                f"0_loss_II: {loss_dis[3][2]:.2%},  ", 
                f"0_loss_TT: {loss_dis[4][2]:.2%}" )
    print()

def output_dis(s, dis):
    if False:
        res = s + f"_dis_PosNeg:<{dis[0]:.0f}--{dis[1]:.0f}>, "
    else:
        res = s + f"_dis_PosNeg:<{dis[0]:.6f}--{dis[1]:.6f}>, "
    return res

def printLossLog(res):

    pbar = tqdm(range(len(res)))
    for e in pbar:
        dis = res[e][0]
        output_loss_dis(f"epoch {e}: 1- <training dataset> in train-model", dis)
        
        loss_dis_valid = res[e][1]   
        output_loss_dis(f"epoch {e}: 2- <validation dataset> in evalue-model", loss_dis_valid)

def print_tags(data, tag_v):
    tags = [data.tag_list[i] for i in range(len(tag_v)) if tag_v[i] ]
    print(tags)

def write_loss_log(options, writer, res, name, index):
    #loss, IT_positive_dis, II_positive_dis, IT_negative_dis, II_negative_dis
    writer.add_scalar('Loss/' + name, res[0], index)
    if options.loss_function != "cross_entropy":
        if res[1] != 0:
            writer.add_scalar('0_IT_Loss/' + name, res[1][2], index)
        if res[2] != 0:
            writer.add_scalar('0_TI_Loss/' + name, res[2][2], index)
        if res[3] != 0:
            writer.add_scalar('0_II_Loss/' + name, res[3][2], index)
        if res[4] != 0:
            writer.add_scalar('0_TT_Loss/' + name, res[4][2], index)

def show_data(data, number=10):

    rows = random.sample(range(data.image_number), number)
    for r in rows:
        print("index:", r)
        print("Ground Truth:")
        print_tags(data, data.image_tags[r])
        display.display(display.Image(data.get_image_path_by_index(r)))

def show_data_with_indexes(data, rows):

    for r in rows:
        print("Ground Truth:")
        print_tags(data, data.image_tags[r])
        display.display(display.Image(data.get_image_path_by_index(r)))



