from gensim.models import Word2Vec
import gensim.downloader
import os
import torch


def compute_sentence_list(tags_list, end_symbol):
    sl = []
    for i in range(len(tags_list)):
        s = []
        n = len(tags_list[0])
        for j in range(len(tags_list[0])):
            if tags_list[i][j]:
                s.append(str(j))
        if end_symbol:
            s.append(str(n))
        sl.append(s)
    return sl

def compute_word_matrix(type, tags_list, word_dims, end_symbol=False):
    if type == "Word2Vec":
        sentence_list = compute_sentence_list(tags_list, end_symbol)
        model = Word2Vec(sentences=sentence_list, vector_size=word_dims, window=12, min_count=1, workers=4, epochs=20)
        res = []
        for i in range(len(model.wv)):
            res.append(model.wv[str(i)].tolist())
        return torch.tensor(res).float()

    elif type == "glove.twitter":
        # dim = 25 50 100 200
        Word_Vector_Path = 'glove.twitter.27B/glove.twitter.27B.'
        Processed_Word_Matrix_Path = 'glove.twitter.27B/WordMatrix'
        matrix_path = Processed_Word_Matrix_Path + str(word_dims) + ".txt"

        if os.path.isfile(matrix_path):
            matrix = [line.strip().split() for line in open(matrix_path).readlines()]
            matrix = [list( map(float,i) ) for i in matrix]
        else:
            full_matrix_path = Word_Vector_Path + str(word_dims) + "d.txt"
            full_matrix = [line.strip().split() for line in open(full_matrix_path,'rb').readlines()]
            m = [full_matrix[i] for i in range(len(full_matrix)) if full_matrix[i][0].decode("utf-8") in tags_list]

            matrix = [[] for i in range(len(tags_list))]
            for i in range(len(tags_list)):
                pos = tags_list.index(m[i][0].decode("utf-8"))
                matrix[pos] = [float(_.decode("utf-8")) for _ in m[i][1:]]

            with open(matrix_path, 'w') as f:
                for i in range(len(matrix)):
                    for value in matrix[i]:
                        f.write(str(value) + " ")
                    f.write("\n")
        return torch.tensor(matrix).float()

    elif type == "clip":
        return 

