import torch
import random


if torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

def get_multy_tag_indexes(tags):
    indexes = []
    for tag in tags:
        indexes.append(torch.nonzero(tag).view(-1).tolist())
    return indexes

def get_index_matrix(tag_matrix, padding_index, max_length, padding_mask=False, eos=False):

    if padding_mask:
        src_key_padding_mask = torch.ones((tag_matrix.shape[0], max_length + int(eos))).to(device)
    
    all_indeces = get_multy_tag_indexes(tag_matrix)
    index_matrix = []
    length_list = []

    for i in range(tag_matrix.shape[0]):
        indeces = all_indeces[i]
        length_list.append(min(max_length, len(indeces)))
        if len(indeces) > max_length:
            indeces = random.sample(indeces, max_length)
        if padding_mask:
            if eos:
                src_key_padding_mask[i][0:len(indeces)+1] = torch.tensor([0] * (len(indeces)+1))
            else:
                src_key_padding_mask[i][0:len(indeces)] = torch.tensor([0] * len(indeces))

        indeces.extend([padding_index] * (max_length - len(indeces) + int(eos)))
        index_matrix.append(indeces)

    index_matrix = torch.tensor(index_matrix).to(device)
    length_list = torch.tensor(length_list).to(device)

    if padding_mask:
        return index_matrix, length_list, src_key_padding_mask
    else:
        return index_matrix, length_list