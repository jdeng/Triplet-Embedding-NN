import torch.nn as nn

from .model_SCNN import *
from .model_Transformer import *
from .model_EmbeddingNet import *
from .model_FcNet import *
from .model_Transformer_clip import *

class Tag_Model(nn.Module): # input batchSize * 1 * tagNum * tagNum
    def __init__(self, options, tag_list, word_embedding_matrix, feature_dim=1000, max_batch_size=512):
        super().__init__()
        self.max_batch_size = max_batch_size
        self.tag_num = len(tag_list)
        self.max_length = options.tag_max_length

        if options.tag_model == "SCNN":
            filters=list(range(3, 3 + options.filter_size_num_SCNN))
            filter_num=[options.filter_num_SCNN] * options.filter_size_num_SCNN
            print("filters", filters)
            print("filter_num", filter_num)
            self.model = SCNN(options, self.tag_num, feature_dim, filters, 
                                filter_num, word_matrix=word_embedding_matrix).to(device)
        elif options.tag_model == "Transformer":
            self.model = TransformerModel(options, self.tag_num, feature_dim, word_embedding_matrix)
        elif options.tag_model == "Transformer_clip":
            self.model = TransformerModel_clip(options, tag_list, feature_dim)
        elif options.tag_model == "Embedding":
            self.model = EmbeddingNet(self.tag_num, feature_dim)
        elif options.tag_model == "FC":
            self.model = FcNet(self.tag_num, feature_dim)

    def compute_features(self, tag_matrix):
        n = len(tag_matrix)
        bs = self.max_batch_size
        tag_features = []
        for i in range(math.ceil(n/bs)):
            tag_features.append(self.model(tag_matrix[i*bs:min(i*bs+bs, n)]))
        tag_features = torch.cat(tag_features)
        return tag_features

    def compute_single_tag_feature(self):
        n = self.tag_num
        single_tag_matrix = torch.eye(n).to(device) 
        single_tag_features = self.compute_features(single_tag_matrix)
        return single_tag_features, single_tag_matrix

    def compute_2_tags_feature(self):
        n = self.tag_num
        single_tag_matrix = torch.eye(n).to(device)
        single_tag_features = self.compute_features(single_tag_matrix)
        two_tags_feature_list = []
        for i in range(n):
            two_tag_i = single_tag_matrix.clone()
            two_tag_i[:,i] = 1
            two_tags_feature_list.append(self.compute_features(two_tag_i))
        return single_tag_features, torch.stack(two_tags_feature_list)


    def forward(self, x=None, tag_sets=None):
        if tag_sets == None:
            out = self.model(x)
        else:
            out = self.model(tag_sets=tag_sets)
        return out



