from enum import IntEnum
import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

from .Utils import *

class EbeddingModel (IntEnum):
    # tag -> feature ->tag
    static = 0
    # image -> feature ->tag
    unstatic = 1
    multichannel = 2


class GlobalMaxPool2d(nn.Module):
    def __init__(self):
        super().__init__()
    def forward(self, x):
        return F.max_pool2d(x, kernel_size=x.size()[2:])

class BasicConv2d(nn.Module):

    def __init__(self, in_channels, out_channels, **kwargs):
        super(BasicConv2d, self).__init__()
        self.conv = nn.Conv2d(in_channels, out_channels, **kwargs)
        self.bn = nn.BatchNorm2d(out_channels)

    def forward(self, x):
        x = self.conv(x)
        x = self.bn(x)
        return F.relu(x)

class Conv2dSeq(nn.Module):

    def __init__(self, layer_num, in_channels, out_channels, kernel_size, **kwargs):
        super(Conv2dSeq, self).__init__()
        self.NUM = layer_num
        for i in range(self.NUM):
            conv = BasicConv2d(in_channels, out_channels, kernel_size=kernel_size, **kwargs)
            setattr(self, f'conv_{i}', conv)

    def get_conv(self, i):
        return getattr(self, f'conv_{i}')

    def forward(self, x):
        for i in range(self.NUM):
            x = self.get_conv(i)(x)
        return x

class SCNN(nn.Module): # input batchSize * 1 * tagNum * tagNum 
    def __init__(self, options, tag_num, feature_dims, filters, filter_num, word_matrix, dropout_prob=0.5):
        super().__init__()
        
        self.Feature_Dimensions = feature_dims
        self.word_dims = word_matrix.shape[1]

        self.tag_num = tag_num
        self.vocab_size = tag_num
        self.DROPOUT_PROB = dropout_prob
        self.IN_CHANNEL = 2
        self.FILTERS = filters
        self.filter_num = filter_num
        self.max_length = options.tag_max_length
        self.depth = options.depth_of_SCNN

        self.embedding_unstatic = nn.Embedding(self.vocab_size +1, self.word_dims, padding_idx=self.vocab_size)
        
        if self.IN_CHANNEL == 2:
            self.embedding_static = nn.Embedding(self.vocab_size +1, self.word_dims, padding_idx=self.vocab_size)
            self.WV_MATRIX = torch.cat((word_matrix, torch.zeros((1,self.word_dims))), 0)
            self.embedding_static.weight.data.copy_(self.WV_MATRIX)
            self.embedding_static.weight.requires_grad = False           

        for i in range(len(self.FILTERS)):
            if self.depth == 1:
                conv = nn.Sequential(
                    BasicConv2d(2, self.filter_num[i], kernel_size=(self.FILTERS[i], self.word_dims)))
            elif self.depth == 2:
                conv = nn.Sequential(
                    BasicConv2d(2, self.filter_num[i], kernel_size=(self.FILTERS[i], self.word_dims)),
                    BasicConv2d(self.filter_num[i], 4 * self.filter_num[i], kernel_size=1) )
            else:
                conv = nn.Sequential(
                    BasicConv2d(2, self.filter_num[i], kernel_size=(self.FILTERS[i], self.word_dims)),
                    BasicConv2d(self.filter_num[i], 4 * self.filter_num[i], kernel_size=1),
                    Conv2dSeq(self.depth-2, 4*self.filter_num[i], 4*self.filter_num[i], kernel_size=1) )
            setattr(self, f'conv_{i}', conv)

        self.pooling = GlobalMaxPool2d()
        
        if self.depth != 1:

            '''self.fc = nn.Sequential( 
                nn.Dropout(p=self.DROPOUT_PROB, inplace=False),
                nn.Linear(4 * sum(self.filter_num), out_features=4096, bias=True),
                nn.BatchNorm1d(4096),
                nn.ReLU(inplace=True),
                nn.Dropout(p=self.DROPOUT_PROB, inplace=False),
                nn.Linear(in_features=4096, out_features=self.Feature_Dimensions, bias=True),
                )'''

            self.fc = nn.Sequential( 
                nn.Dropout(p=self.DROPOUT_PROB, inplace=False),
                nn.Linear(4 * sum(self.filter_num), out_features=self.Feature_Dimensions, bias=True),
                )
        else:
            self.fc = nn.Sequential( 
                nn.Dropout(p=self.DROPOUT_PROB, inplace=False),
                nn.Linear(sum(self.filter_num), out_features=self.Feature_Dimensions, bias=True),
                )

    def get_conv(self, i):
        return getattr(self, f'conv_{i}')

    def forward(self, tag_matrix):

        index_matrix= get_index_matrix(tag_matrix, self.vocab_size, self.max_length)[0]
        x = self.embedding_unstatic(index_matrix).view(len(tag_matrix), 1, -1, self.word_dims)

        if self.IN_CHANNEL == 2:
            x2 = self.embedding_static(index_matrix).view(len(tag_matrix), 1, -1, self.word_dims)
            x = torch.cat((x, x2), 1)

        out = [self.pooling(self.get_conv(i)(x)).view(tag_matrix.shape[0], -1)  for i in range(len(self.FILTERS))]
        out = torch.cat(out, 1)
        out = self.fc(out)
        return F.normalize(out)
