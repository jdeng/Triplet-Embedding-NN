from torch import nn
import torch.nn.functional as F

from .Utils import *


class EmbeddingNet(nn.Module):

    def __init__(self, ntag, feature_dim, base_token_num=200, token_embedding_dim=1024):
        super().__init__()
        self.model_type = 'Embedding'
        self.tag_num = ntag
        self.feature_dim = feature_dim
        self.token_embedding_dim = token_embedding_dim
        self.base_token_num = base_token_num
        self.tag_token_mapping = nn.Embedding(self.tag_num, self.base_token_num)
        self.token_embedding = nn.Embedding(self.base_token_num, self.token_embedding_dim)
        self.feature_embedding = nn.Embedding(feature_dim, self.token_embedding_dim)

    def forward(self, tag_matrix):
        a = self.tag_token_mapping.weight.clone()
        b = self.token_embedding.weight.clone()
        c = self.feature_embedding.weight.clone()
        output = tag_matrix.float() @ a @ b @ c.t()
        return F.normalize(output)
