from torch import nn
import torch.nn.functional as F

from .Utils import *


class FcNet(nn.Module):

    def __init__(self, ntag, feature_dim, dropout_p=0.5):
        super().__init__()
        self.model_type = 'Fc'
        self.tag_num = ntag
        self.feature_dim = feature_dim
        self.fc =  nn.Sequential( 
                                nn.Linear(ntag, 1024),
                                nn.BatchNorm1d(1024),
                                nn.ReLU(),

                                nn.Linear(1024, 1024),
                                nn.BatchNorm1d(1024),
                                nn.ReLU(),
                                
                                nn.Linear(1024, self.feature_dim)
                                )

    def forward(self, tag_matrix):
        output = self.fc(tag_matrix.float())
        return F.normalize(output)
