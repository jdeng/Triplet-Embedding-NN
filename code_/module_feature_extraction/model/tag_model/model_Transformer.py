import math
from typing import Tuple

import torch
from torch import nn, Tensor
import torch.nn.functional as F
from torch.nn import TransformerEncoder, TransformerEncoderLayer
from torch.utils.data import dataset

from .Utils import *

class TransformerModel(nn.Module):

    def __init__(self, options, ntag, feature_dim, token_embedding_matrix, dropout=0.5):
        super().__init__()
        self.model_type = 'Transformer'

        #batch_first=True
        nhead = options.transformer_nhead
        self.d_hid = options.transformer_d_hid
        # d_model  the number of expected features in the encoder/decoder inputs
        self.token_embedding_dim = token_embedding_matrix.shape[1]
        encoder_layers = TransformerEncoderLayer(self.token_embedding_dim, nhead, self.d_hid, dropout, batch_first=True)
        nlayers = options.transformer_nlayers
        self.transformer_encoder = TransformerEncoder(encoder_layers, nlayers)
        
        self.ntoken = ntag + 1
        self.feature_dim = feature_dim
        self.max_length = options.tag_max_length
        self.eos_index = self.ntoken-1

        self.sentence_feature = options.sentence_feature
        self.fc_layer = options.fc_layer
        if self.sentence_feature == "eos":
            self.token_embedding = nn.Embedding(self.ntoken, self.token_embedding_dim)
            self.token_embedding.weight.data.copy_(token_embedding_matrix)
        else:
            self.token_embedding = nn.Embedding(self.ntoken, self.token_embedding_dim, padding_idx=self.ntoken-1)
            token_embedding_matrix[-1] = token_embedding_matrix[-1] * 0
            self.token_embedding.weight.data.copy_(token_embedding_matrix)

        if False and self.fc_layer:
            self.fc = nn.Sequential( 
                nn.Dropout(p=dropout, inplace=False),
                nn.Linear(self.token_embedding_dim, feature_dim, bias=True)
                )
        elif self.fc_layer:
            self.fc = nn.Linear(self.token_embedding_dim, feature_dim, bias=True)
            

    def forward(self, tag_matrix):
        """
        Args:
            tag_matrix: Tensor, shape [batch_size, ntoken-1]

        Returns:
            output Tensor of shape [batch_size, feature_dim]
        """
        index_matrix, eos_position, padding_mask =get_index_matrix(tag_matrix, self.eos_index, 
                            self.max_length, padding_mask=True, eos=self.sentence_feature=="eos")

        # [batch_size, max_length, token_embedding_dim]
        token_embedding_matrix = self.token_embedding(index_matrix)
        output = self.transformer_encoder(token_embedding_matrix, src_key_padding_mask=padding_mask)
        #print(output.shape)
        if self.sentence_feature == "eos":
            output = torch.stack([output[i,eos_position[i]] for i in range(len(output))])
        elif self.sentence_feature == "sum":
            output = torch.sum(output, dim=-2)
        elif self.sentence_feature == "mean":
            output = torch.mean(output, dim=-2)
        elif self.sentence_feature == "max":
            output = torch.max(output, dim=-2)[0]
        #print(output.shape)
        if self.fc_layer:
            output = self.fc(output)
        return F.normalize(output)