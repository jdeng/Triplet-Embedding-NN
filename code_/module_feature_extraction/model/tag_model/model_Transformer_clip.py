
import clip
from clip.clip import _tokenizer
import torch
from torch import nn, Tensor
import torch.nn.functional as F
from torch.nn import TransformerEncoder, TransformerEncoderLayer
from torch.utils.data import dataset

from .Utils import *

class TransformerModel_clip(nn.Module):

    def __init__(self, options, tag_list, feature_dim, dropout=0.5):
        super().__init__()
        self.model_type = 'Transformer_clip'
        self.tag_list = tag_list
        #batch_first=True
        nhead = options.transformer_nhead
        self.d_hid = options.transformer_d_hid
        token_embedding_matrix = self.compute_embedding_matrix()

        # d_model  the number of expected features in the encoder/decoder inputs
        self.token_embedding_dim = token_embedding_matrix.shape[1]
        encoder_layers = TransformerEncoderLayer(self.token_embedding_dim, nhead, self.d_hid, dropout, batch_first=True)
        nlayers = options.transformer_nlayers
        self.transformer_encoder = TransformerEncoder(encoder_layers, nlayers)
        
        self.feature_dim = feature_dim
        self.max_length = options.tag_max_length
        self.ntoken = token_embedding_matrix.shape[0]
        self.eos_index = self.ntoken-1

        self.sentence_feature = options.sentence_feature
        self.fc_layer = options.fc_layer
        self.token_embedding = nn.Embedding(self.ntoken, self.token_embedding_dim, padding_idx=self.ntoken-1)
        self.token_embedding.weight.data.copy_(token_embedding_matrix)
        
        self.token_embedding.weight.requires_grad = False  
        if self.fc_layer:
            self.fc = nn.Linear(self.token_embedding_dim, feature_dim)

    def compute_embedding_matrix(self):
        model, preprocess = clip.load("ViT-B/32", device=device)
        x = model.token_embedding.weight.clone().to(device)
        return x

    def encoding_tag_set(self, tag_set):
        tokens = []
        self.max_length
        tag_set = [tag_set[index] for index in random.sample(range(len(tag_set)), len(tag_set))]
        for tag in tag_set:
            temp = _tokenizer.encode(tag)
            if len(tokens) + len(temp) + 1 <= self.max_length:
                tokens = tokens + temp
            else:
                tokens = tokens + [self.eos_index]
                break
        return tokens

    def encoding_tag_sets(self, tag_sets):
        index_matrix = []
        length_list = []
        src_key_padding_mask =  torch.ones((len(tag_sets), self.max_length)).to(device)
        for i, ts in enumerate(tag_sets):
            temp = self.encoding_tag_set(ts)
            length_list.append(len(temp))
            src_key_padding_mask[i][0:len(temp)] = torch.tensor([0] * (len(temp)))
            temp.extend([self.eos_index] * (self.max_length - len(temp)))
            index_matrix.append(temp)
        index_matrix = torch.tensor(index_matrix).to(device)
        length_list = torch.tensor(length_list).to(device)
        return  index_matrix, length_list-1, src_key_padding_mask

    def encoding_tag_matrix(self, tag_matrix):
        tag_sets = []
        for tag_v in tag_matrix:
            indices = torch.nonzero(tag_v).view(-1).tolist()
            tag_sets.append([self.tag_list[index] for index in indices])
        return  self.encoding_tag_sets(tag_sets)


    def forward(self, tag_matrix=None, tag_sets=None):
        """
        Args:
            tag_matrix: Tensor, shape [batch_size, ntoken-1]

        Returns:
            output Tensor of shape [batch_size, feature_dim]
        """
        if tag_sets == None:
            index_matrix, eos_position, padding_mask = self.encoding_tag_matrix(tag_matrix)
        else:
            index_matrix, eos_position, padding_mask = self.encoding_tag_sets(tag_sets)

        # [batch_size, max_length, token_embedding_dim]
        token_embedding_matrix = self.token_embedding(index_matrix)
        output = self.transformer_encoder(token_embedding_matrix, src_key_padding_mask=padding_mask)
        #print(output.shape)
        if self.sentence_feature == "eos":
            output = torch.stack([output[i,eos_position[i]] for i in range(len(output))])
        elif self.sentence_feature == "sum":
            output = torch.sum(output, dim=-2)
        elif self.sentence_feature == "mean":
            output = torch.mean(output, dim=-2)
        elif self.sentence_feature == "max":
            output = torch.max(output, dim=-2)[0]
        #print(output.shape)
        if self.fc_layer:
            output = self.fc(output)
        return F.normalize(output)