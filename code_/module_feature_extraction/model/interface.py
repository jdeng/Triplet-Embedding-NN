import os
import torch
import torch.nn.functional as F
from tqdm.notebook import tqdm
import argparse

from .Visualization import *
from .Utils import *

from code_ import module_prediction as P

model_path = './model_state/'
max_IT_0loss = 0
min_loss = 100

def save_model(image_model, tag_model, name):
    torch.save({
            'image_model_state_dict': image_model.state_dict(),
            'tag_model_state_dict': tag_model.state_dict(),
            }, name)

def load_model(tag_model, image_model, name):
    try:
        checkpoint = torch.load(name)
        image_model.load_state_dict(checkpoint['image_model_state_dict'])   
        tag_model.load_state_dict(checkpoint['tag_model_state_dict'])        # 从字典中依次读取
        print("Load last checkpoint data")
        return True
    except FileNotFoundError:
        print("Can\'t found " + name)
        return False

def run(writer, image_model, tag_model, decoder, train_loader, valid_loader, train_loader_E, valid_loader_E, 
        test_loader_E, loss_func, optimizer, scheduler, options, print_log=True):

    n_epochs =  options.epoch
    pbar = tqdm(range(n_epochs))
    #max_global_magnification = options.global_magnification
    #options.global_magnification = 1
    
    for e in pbar:

        loss_dis_train = train(image_model, tag_model, train_loader, loss_func, optimizer, options)
        #loss_dis_train_2 = validate(image_model, tag_model, train_loader, loss_func, optimizer, options) 
        loss_dis_valid = validate(image_model, tag_model, valid_loader, loss_func, optimizer, options) 
        

        if print_log:
            print(f"epoch {e}: #####################################################################")
        print()

        write_loss_log(options, writer, loss_dis_train, "train", e)
        write_loss_log(options, writer, loss_dis_valid, "valid", e)

        if print_log:
            print("----1----  <training dataset> in train-model:")
            output_loss_dis(loss_dis_train, options) 
            print()
            '''
            print("----1----  <training dataset> in evaluate-model:")
            output_loss_dis(loss_dis_train_2, options) 
            print()
            '''
            print("----2----  <validation dataset> in evaluate-model")
            output_loss_dis(loss_dis_valid, options)
            print()
        train_loader_E.dataset.turn_off_data_augmentation()
        evaluate_kclosest(train_loader_E, valid_loader_E, test_loader_E, image_model, tag_model, writer, e, k=options.k_closest_tags)
        evaluate_pos_closer(train_loader_E, valid_loader_E, test_loader_E, image_model, tag_model, writer, e)
        evaluate_search(train_loader_E, valid_loader_E, test_loader_E, image_model, tag_model, writer, e)
        train_loader_E.dataset.turn_on_data_augmentation()
        
        #if proportion_0_losses[3] > 0.9:
        #    options.global_magnification = min(max_global_magnification, options.global_magnification+1)

        if scheduler != None:
            scheduler[0].step()
            scheduler[1].step()

        if options.decoder_evaluate and (options.test or e % 6 == 0 and e != 0):
            #options, train_both, writer, decoder, image_model, train_loader, valid_loader, lr, n_epochs=1, index=-1
            P.run(options, False, writer, decoder, image_model, tag_model, train_loader, valid_loader, 
                    train_loader_E, valid_loader_E, test_loader_E, n_epochs=3, index=e)
        
        global min_loss
        print("min_loss:", min_loss, ",    loss:", loss_dis_valid[0])
        if len(options.model_name) != 0 and (e == 0 or min_loss > loss_dis_valid[0]):
            min_loss = loss_dis_valid[0]
            print("save model")
            if not os.path.exists(model_path):
                os.makedirs(model_path)
            save_model(image_model, tag_model, model_path + options.model_name)
    return

def train(image_model, tag_model, loader, loss_func, optim, options, updata=True):

    image_model.train()
    tag_model.train()

    res = single_epoch_computation(image_model, tag_model, loader, loss_func, options, optim, updata=True)

    return res

def validate(image_model, tag_model, loader, loss_func, optim, options):
    
    image_model.eval()
    tag_model.eval()
    with torch.no_grad():
        res = single_epoch_computation(image_model, tag_model, loader, loss_func, options, optim, updata=False)
    return res

def evaluate_kclosest(train_loader, valid_loader, test_loader, image_model, tag_model, writer, index, k):
    evaluation_res_train = P.evaluate_with_k_closest(train_loader, image_model, tag_model, k)
    evaluation_res_valid = P.evaluate_with_k_closest(valid_loader, image_model, tag_model, k)
    evaluation_res_test = P.evaluate_with_k_closest(test_loader, image_model, tag_model, k)
    P.print_evaluation("training", evaluation_res_train, "k closest")
    P.print_evaluation("validation", evaluation_res_valid, "k closest")
    P.print_evaluation("testing", evaluation_res_test, "k closest")
    P.write_evaluation_log(writer, evaluation_res_train, "train", index, "k-closest_")
    P.write_evaluation_log(writer, evaluation_res_valid, "valid", index, "k-closest_")
    P.write_evaluation_log(writer, evaluation_res_test, "test", index, "k-closest_")

def evaluate_pos_closer(train_loader, valid_loader, test_loader, image_model, tag_model, writer, index):
    evaluation_res_train = P.evaluate_with_pos_closer(train_loader, image_model, tag_model)
    evaluation_res_valid = P.evaluate_with_pos_closer(valid_loader, image_model, tag_model)
    evaluation_res_test = P.evaluate_with_pos_closer(test_loader, image_model, tag_model)
    P.print_evaluation("training", evaluation_res_train, "pos closer")
    P.print_evaluation("validation", evaluation_res_valid, "pos closer")
    P.print_evaluation("testing", evaluation_res_test, "pos closer")
    P.write_evaluation_log(writer, evaluation_res_train, "train", index, "pos_closer_")
    P.write_evaluation_log(writer, evaluation_res_valid, "valid", index, "pos_closer_")
    P.write_evaluation_log(writer, evaluation_res_test, "test", index, "pos_closer_")

def evaluate_search(train_loader, valid_loader, test_loader, image_model, tag_model, writer, index):
    evaluation_res_train = P.evaluate_with_search(train_loader, image_model, tag_model)
    evaluation_res_valid = P.evaluate_with_search(valid_loader, image_model, tag_model)
    evaluation_res_test = P.evaluate_with_search(test_loader, image_model, tag_model)
    P.print_evaluation("training", evaluation_res_train, "search")
    P.print_evaluation("validation", evaluation_res_valid, "search")
    P.print_evaluation("testing", evaluation_res_test, "search")
    P.write_evaluation_log(writer, evaluation_res_train, "train", index, "search_")
    P.write_evaluation_log(writer, evaluation_res_valid, "valid", index, "search_")
    P.write_evaluation_log(writer, evaluation_res_test, "test", index, "search_")

