from .model import *



def count_parameters(model):
        return sum(p.numel() for p in model.parameters() if p.requires_grad)
def output_parameter_numbers(options, tag_list, feature_dim, image_tags):
    model = Image_Model("VGG16")
    #torchsummary.summary(model, (1, 512, 512))
    print('VGG16           parameters_count:', count_parameters(model))

    model = Image_Model("ResNet18")
    print('ResNet18        parameters_count:', count_parameters(model))

    model = Image_Model("ResNet34")
    print('ResNet34        parameters_count:', count_parameters(model))

    model = Image_Model("ResNet50")
    print('ResNet50        parameters_count:', count_parameters(model))

    model = Image_Model("ResNet101")
    print('ResNet101       parameters_count:', count_parameters(model))
    
    model = Image_Model("EfficientNet_b3")
    print('EfficientNet_b3 parameters_count:', count_parameters(model))
    
    model = Image_Model("EfficientNet_b4")
    print('EfficientNet_b4 parameters_count:', count_parameters(model))

    model = Image_Model("EfficientNet_b6")
    print('EfficientNet_b6 parameters_count:', count_parameters(model))

    filters=[3, 4, 5]
    filter_num=[100, 100, 100]
    word_dimensions = options.word_dimensions
    word_embedding_matrix = compute_word_matrix("Word2Vec", image_tags, word_dimensions, False)
    model = SCNN(options, tag_list, feature_dim, filters, 
                                filter_num, word_matrix=word_embedding_matrix)
    print('SCNN            parameters_count:', count_parameters(model))

    model = EmbeddingNet(len(tag_list), feature_dim)
    print('EmbeddingNet    parameters_count:', count_parameters(model))

    word_embedding_matrix = compute_word_matrix("Word2Vec", image_tags, word_dimensions, True)
    model = TransformerModel(options, len(tag_list), feature_dim, word_embedding_matrix)
    print('Transformer     parameters_count:', count_parameters(model))