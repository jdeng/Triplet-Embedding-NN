
import math
from turtle import position
from click import option
import torch 
import torch.nn as nn 
import torch.nn.functional as F

if torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

def compute_pairwise_difference(a):
    b = torch.unsqueeze(a, len(a.shape))
    c = torch.unsqueeze(a, len(a.shape)-1)
    return b-c

def compute_feature_distance(featrues_1, features_2):
    return 1 - torch.mm(featrues_1, features_2.t())

def get_similarity_matrix(tag_matrix_1, tag_matrix_2):
    t1 = tag_matrix_1.float()
    t2 = tag_matrix_2.float()
    return torch.mm(t1, t2.t()).int()

class TripletLossFunc(nn.Module): 

    def __init__(self, options): 
        super().__init__() 
        self.individual_triplet_dis = len(options.margin_distance) == 4
        self.alpha = torch.tensor(options.margin_distance).to(device).float()
        if self.individual_triplet_dis:
            self.print_margin_distance()
        self.options = options
        self.batch_all = options.batch_all

    def print_margin_distance(self):
        
        print(f"    IT: {self.alpha[0]:.2f}", 
              f"    TI: {self.alpha[1]:.2f}",
              f"    II: {self.alpha[2]:.2f}",
              f"    TT: {self.alpha[3]:.2f}")
    
    def sample_type(self, anchor_tag_matrix, sample_tag_matrix):
        #pos 1, semi-pos 0, neg -1
        pos = torch.min(anchor_tag_matrix[:,None,:] - sample_tag_matrix[None,:,:], dim=-1)[0] >= 0
        pos_semi = torch.max(anchor_tag_matrix[:,None,:] + sample_tag_matrix[None,:,:], dim=-1)[0] >= 2
        neg = torch.logical_not(pos_semi)
        return pos.int() - neg.int()

    def get_positive_samples(self, anchor_tag_matrix, sample_tag_matrix, sample_features):
        same_tag_num = torch.sum((anchor_tag_matrix[:,None,:] + sample_tag_matrix[None,:,:]) == 2, dim=-1)
        diff_tag_0 = torch.sum((anchor_tag_matrix[:,None,:] + sample_tag_matrix[None,:,:]) == 1, dim=-1) == 0
        position_pos = torch.topk(same_tag_num * diff_tag_0, k=1, dim=-1)[1].squeeze()
        return sample_features[position_pos]

    def get_nearest_negative_samples(self, anchor_features, sample_type, sample_features):
        neg_sim = torch.mm(anchor_features, sample_features.t()) * (sample_type == -1)
        nearest_neg_pos = torch.topk(neg_sim, k=1, dim=-1)[1].squeeze()
        return sample_features[nearest_neg_pos]

    def mean_of_nonzero(self, matrix, dim):
        nonzero_num = torch.count_nonzero(matrix, dim=dim)
        sum = torch.sum(matrix, dim=dim)
        mean_loss = (sum / (nonzero_num + 0.000001)).view(-1,1,1)
        return mean_loss
  
    def forward(self, anchor_tag_matrix, sample_tag_matrix, anchor_features, sample_features, loss_type): 
        if self.individual_triplet_dis:
            alpha = self.alpha[loss_type]
        else:
            alpha = self.alpha[0]
        with torch.no_grad():
            sample_type = self.sample_type(anchor_tag_matrix, sample_tag_matrix)

        positive_features = self.get_positive_samples(anchor_tag_matrix, sample_tag_matrix, sample_features)
        neg_matrix = sample_type == -1
        similarity_matrix_bn = torch.mm(anchor_features, sample_features.t())
        loss_matrix_bn = similarity_matrix_bn + alpha - torch.sum(anchor_features*positive_features,dim=-1).view(-1,1)
        loss_matrix_bn = loss_matrix_bn * neg_matrix
        loss_matrix_bn = torch.clip(loss_matrix_bn, min=0)

        if self.batch_all:
            loss = self.mean_of_nonzero(loss_matrix_bn,dim=-1)
        else:
            loss = torch.topk(loss_matrix_bn, k=1, dim=-1)[0]
           
        proportion_0_loss = (torch.sum(neg_matrix) - torch.count_nonzero(loss_matrix_bn)) / (torch.sum(neg_matrix) + 0.000001)
        neg_dis = torch.sum(neg_matrix * (1-similarity_matrix_bn), dim=-1) / (torch.sum(neg_matrix, dim=-1) + 0.000001)
        pos_dis = torch.sum(anchor_features * positive_features,dim=-1)
        
        return loss.mean(), pos_dis.mean().item(), neg_dis.mean().item(), proportion_0_loss