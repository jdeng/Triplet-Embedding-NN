from numpy import dtype
import torch 
import torch.nn as nn 
import torch.nn.functional as F

if torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

class CrossEntropyLossFunc(nn.Module): 
    def __init__(self, options): 
        super().__init__()
        self.cross_entropy_replace = options.cross_entropy_replace

    def sample_type(self, anchor_tag_matrix, sample_tag_matrix):
        #pos 1, semi-pos 0, neg -1
        pos = torch.min(anchor_tag_matrix[:,None,:] - sample_tag_matrix[None,:,:], dim=-1)[0] >= 0
        pos_semi = torch.max(anchor_tag_matrix[:,None,:] + sample_tag_matrix[None,:,:], dim=-1)[0] >= 2
        neg = torch.logical_not(pos_semi)
        return pos.int() - neg.int()

    def get_positive_value_indices(self, anchor_tag_matrix, sample_tag_matrix):
        same_tag_num = torch.sum((anchor_tag_matrix[:,None,:] + sample_tag_matrix[None,:,:]) == 2, dim=-1)
        diff_tag_0 = torch.sum((anchor_tag_matrix[:,None,:] + sample_tag_matrix[None,:,:]) == 1, dim=-1) == 0
        pos_indices = torch.topk(same_tag_num * diff_tag_0, k=1, dim=-1)[1]
        return pos_indices

    def compute_loss(self, similarity_matrix, neg_similarity_matrix, neg_matrix, pos_indices):
        with torch.no_grad():
            mean_neg_loss = torch.sum(neg_similarity_matrix, dim=-1) / (torch.sum(neg_matrix, dim=-1)+0.000001)
            mean_neg_loss = mean_neg_loss.view(-1,1).expand(neg_matrix.shape)
        if self.cross_entropy_replace == 'mean':
            one_pos_similarity_matrix = torch.where(neg_matrix, neg_similarity_matrix, mean_neg_loss)
        elif self.cross_entropy_replace == '0':
            one_pos_similarity_matrix = neg_similarity_matrix
        else:
            one_pos_similarity_matrix = similarity_matrix
        condition = torch.zeros(neg_matrix.shape, dtype=int).to(device)
        condition.scatter_(dim=1, index=pos_indices, src=pos_indices*0+1)
        one_pos_similarity_matrix = torch.where(condition.bool(), similarity_matrix, one_pos_similarity_matrix)
        return F.cross_entropy(one_pos_similarity_matrix, pos_indices.view(-1))

    def forward(self, image_features, tag_features_ex, y_tag_matrix, tag_matrix_ex):
        # IT TI
        neg_matrix = self.sample_type(y_tag_matrix, tag_matrix_ex) == -1
        similarity_matrix_bn = (torch.mm(image_features, tag_features_ex.t()) + 1)
        neg_similarity_matrix_bn = similarity_matrix_bn * neg_matrix
        pos_indices = self.get_positive_value_indices(y_tag_matrix, tag_matrix_ex)
        loss_IT = self.compute_loss(similarity_matrix_bn, neg_similarity_matrix_bn, neg_matrix, pos_indices)
        pos_indices = self.get_positive_value_indices(tag_matrix_ex, y_tag_matrix)
        loss_TI = self.compute_loss(similarity_matrix_bn.t(), neg_similarity_matrix_bn.t(), neg_matrix.t(), pos_indices)
        del similarity_matrix_bn
        del neg_similarity_matrix_bn
        # II
        neg_matrix = self.sample_type(y_tag_matrix, y_tag_matrix) == -1
        similarity_matrix_bb = (torch.mm(image_features, image_features.t()) + 1)
        neg_similarity_matrix_bb = similarity_matrix_bb * neg_matrix
        pos_indices = torch.LongTensor(list(range(len(y_tag_matrix)))).to(device).view(-1,1)
        loss_II = self.compute_loss(similarity_matrix_bb, neg_similarity_matrix_bb, neg_matrix, pos_indices)
        # TT
        neg_matrix = self.sample_type(tag_matrix_ex, tag_matrix_ex) == -1
        similarity_matrix_nn = (torch.mm(tag_features_ex, tag_features_ex.t()) + 1)
        neg_similarity_matrix_nn = similarity_matrix_nn * neg_matrix
        pos_indices = torch.LongTensor(list(range(len(tag_matrix_ex)))).to(device).view(-1,1)
        loss_TT = self.compute_loss(similarity_matrix_nn, neg_similarity_matrix_nn, neg_matrix, pos_indices)
        return (loss_IT + loss_TI + loss_II + loss_TT) 