import math
import torch 
import torch.nn as nn 
import torch.nn.functional as F

if torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

def compute_pairwise_difference(a):
    b = torch.unsqueeze(a, len(a.shape))
    c = torch.unsqueeze(a, len(a.shape)-1)
    return b-c

def compute_feature_distance(featrues_1, features_2):
    return 1 - torch.mm(featrues_1, features_2.t())

def get_similarity_matrix(tag_matrix_1, tag_matrix_2):
    t1 = tag_matrix_1.float()
    t2 = tag_matrix_2.float()
    return torch.mm(t1, t2.t()).int()

class StepMetricLossFunc(nn.Module): 

    def __init__(self, options): 
        super().__init__() 
        self.individual_triplet_dis = len(options.margin_distance) == 4
        self.alpha = torch.tensor(options.margin_distance).to(device).float()
        self.step_hard = options.step_hard
        self.step_top = options.step_top
        self.step_softplus = options.step_softplus
        self.step_margin_transform = options.step_margin_transform
        if self.individual_triplet_dis:
            self.print_margin_distance()
        self.options = options
        self.condition_type = options.step_condition_type

    def print_margin_distance(self):
        
        print(f"    IT: {self.alpha[0]:.2f}", 
              f"    TI: {self.alpha[1]:.2f}",
              f"    II: {self.alpha[2]:.2f}",
              f"    TT: {self.alpha[3]:.2f}")
    
    def compute_condition(self, anchor_tag_matrix, sample_tag_matrix):
        tn = len(anchor_tag_matrix[0])
        with torch.no_grad():
            #n*n
            sample_tag_diff = sample_tag_matrix[:,None,:] - sample_tag_matrix[None,:,:]
            st_bn = self.sample_type(anchor_tag_matrix, sample_tag_matrix)
            if self.condition_type == 0:
                # all
                condition = True
            elif self.condition_type == 1:
                # no neg-neg
                condition = (st_bn[:,:,None] + st_bn[:,None,:]) != -2
            elif self.condition_type == 2:
                # no pos-pos, semi-semi, neg-neg
                condition = st_bn[:,:,None] != st_bn[:,None,:]
            bs = 4
            for i in range(math.ceil(len(anchor_tag_matrix[0])/bs)):
                tag_diff_i = anchor_tag_matrix[:,None,None,i*bs:min((i*bs+bs, tn))] - sample_tag_diff[None,:,:,i*bs:min((i*bs+bs, tn))]
                condition = condition & torch.all(tag_diff_i < 2, dim=-1) & torch.all(tag_diff_i > -1, dim=-1)
        return condition ^ torch.eye(len(sample_tag_matrix), dtype=bool).to(device)[None,:,:] 

    def compute_fake_margin_matrix(self, anchor_tag_matrix, sample_tag_matrix):
        fake_anchor_features = F.normalize(anchor_tag_matrix.float())
        fake_sample_features = F.normalize(sample_tag_matrix.float())
        fake_feature_distance_matrix_bn = compute_feature_distance(fake_anchor_features, fake_sample_features)
        return -1 * compute_pairwise_difference(fake_feature_distance_matrix_bn)

    def compute_fake_margin_matrix2(self, anchor_tag_matrix, similarity_matrix_bn, sample_tag_num_matrix_bn):
        
        with torch.no_grad():
            n = len(sample_tag_num_matrix_bn[0])
            batch_tag_num_bnn = torch.sum(anchor_tag_matrix, dim=1)[:, None, None].expand(-1,n,n)
            batch_tn_time_sample_tn_bnn = sample_tag_num_matrix_bn[:, :, None] * batch_tag_num_bnn
            sqrt_tn_time_tn_bnn = torch.sqrt(batch_tn_time_sample_tn_bnn)
            del batch_tag_num_bnn
            del batch_tn_time_sample_tn_bnn
            cosine_pos = (similarity_matrix_bn[:, :, None].expand(-1,n,n) / sqrt_tn_time_tn_bnn)
            cosine_neg = (similarity_matrix_bn[:, None, :].expand(-1,n,n) / sqrt_tn_time_tn_bnn.transpose_(1,2))
            margin_matrix = (cosine_pos - cosine_neg)
            #print(torch.min(margin_matrix), torch.max(margin_matrix))
            del cosine_pos
            del cosine_neg
        
        return margin_matrix 

    def mean_of_nonzero(self, matrix, dim):
        nonzero_num = torch.count_nonzero(matrix, dim=dim)
        sum = torch.sum(matrix, dim=dim)
        mean_loss = (sum / (nonzero_num + 0.000001)).view(-1,1,1)
        return mean_loss

    def sample_type(self, anchor_tag_matrix, sample_tag_matrix):
        #pos 1, semi-pos 0, neg -1
        pos = torch.min(anchor_tag_matrix[:,None,:] - sample_tag_matrix[None,:,:], dim=-1)[0] >= 0
        pos_semi = torch.max(anchor_tag_matrix[:,None,:] + sample_tag_matrix[None,:,:], dim=-1)[0] >= 2
        neg = torch.logical_not(pos_semi)
        return pos.int() - neg.int()
  
    def forward(self, anchor_tag_matrix, sample_tag_matrix, anchor_features, sample_features, loss_type): 
        '''
        anchor_tag_matrix = torch.tensor([[1,1,0,1]]).to(device)
        sample_tag_matrix = torch.tensor([[1,1,0,1], [1,1,0,0], [0,1,1,0], [1,1,1,1], [0,0,1,0]]).to(device)
        anchor_features = F.normalize(anchor_tag_matrix.float())
        sample_features = F.normalize(sample_tag_matrix.float())
        '''
        with torch.no_grad():
            similarity_matrix_bn = get_similarity_matrix(anchor_tag_matrix, sample_tag_matrix)
            sample_tag_num_matrix_bn = torch.sum(sample_tag_matrix, dim=-1).expand(anchor_tag_matrix.shape[0],-1)
            condition_bnn = self.compute_condition(anchor_tag_matrix, sample_tag_matrix)
        if self.individual_triplet_dis:
            alpha = self.alpha[loss_type]
        else:
            alpha = self.alpha[0]
        
        distance_matrix_bn = compute_feature_distance(anchor_features, sample_features)
        loss_matrix_bnn = compute_pairwise_difference(distance_matrix_bn)
        #loss_matrix_bnn = loss_matrix_bnn + alpha * self.compute_margin_matrix(anchor_tag_matrix, similarity_matrix_bn, sample_tag_num_matrix_bn)
        #loss_matrix_bnn = loss_matrix_bnn + alpha * 1/self.options.tag_max_length
        if self.step_margin_transform == "linear":
            margin_matrix_bnn = alpha * self.compute_fake_margin_matrix(anchor_tag_matrix, sample_tag_matrix)
        elif self.step_margin_transform == "sq":
            margin_matrix_bnn = self.compute_fake_margin_matrix(anchor_tag_matrix, sample_tag_matrix)
            margin_matrix_bnn = alpha * margin_matrix_bnn * margin_matrix_bnn
        loss_matrix_bnn = loss_matrix_bnn + margin_matrix_bnn
        '''
        print("condition_bnn", condition_bnn)
        print("margin_matrix",self.compute_margin_matrix(anchor_tag_matrix, sample_tag_matrix)) 
        print("margin_matrix2",self.compute_margin_matrix2(anchor_tag_matrix, similarity_matrix_bn, sample_tag_num_matrix_bn)) 
        print("distance_matrix_bn", distance_matrix_bn)
        print("loss_matrix_bnn",loss_matrix_bnn)
        '''
        loss_matrix_bnn = condition_bnn * loss_matrix_bnn

        if self.step_softplus:
            loss_matrix_bnn = torch.log(1+torch.exp(loss_matrix_bnn))
        else:
            loss_matrix_bnn = torch.clamp(loss_matrix_bnn, min=0)
        num_losses = torch.sum(condition_bnn).item()
        '''
        print("type", loss_type, "num", torch.count_nonzero(loss_matrix_bnn, dim=(1,2)))
        print("mean0", self.mean_of_nonzero(margin_matrix_bnn, dim=(1,2)).view(-1))
        print("mean1", self.mean_of_nonzero(margin_matrix_bnn*condition_bnn, dim=(1,2)).view(-1))
        print("loss", self.mean_of_nonzero(loss_matrix_bnn, dim=(1,2)).view(-1))
        '''
        del condition_bnn
        num_nonzero_losses = torch.count_nonzero(loss_matrix_bnn).item()
        
        if self.step_top > 0 and self.step_top <= len(sample_tag_matrix):
            loss_matrix_bnn = torch.topk(loss_matrix_bnn.view(len(loss_matrix_bnn),-1), k=self.step_top, dim=-1)[0][:,:,None]

        for i in range(self.step_hard):
            loss_matrix_bnn = (loss_matrix_bnn >=  self.mean_of_nonzero(loss_matrix_bnn, dim=(1,2)).view(-1,1,1)) * loss_matrix_bnn
        
        
        loss = torch.sum(loss_matrix_bnn, dim=(1,2)) / (torch.count_nonzero(loss_matrix_bnn, dim=(1,2)) + 0.000001)
    
        if torch.isnan(loss).any():
            print("margin_matrix", torch.isnan(self.compute_margin_matrix()).any())
            print("batch_tag_num", torch.min(torch.sum(anchor_tag_matrix, dim=1)))
            print("sample_tag_num_matrix_bn", torch.min(sample_tag_num_matrix_bn))

        proportion_0_loss = (num_losses - num_nonzero_losses ) / (num_losses + 0.000001)
        neg_matrix = similarity_matrix_bn == 0
        neg_dis = torch.sum(neg_matrix * distance_matrix_bn, dim=-1) / (torch.sum(neg_matrix, dim=-1) + 0.000001)
        pos_matrix = (sample_tag_num_matrix_bn - similarity_matrix_bn) == 0
        pos_dis = torch.sum(pos_matrix * distance_matrix_bn, dim=-1) / (torch.sum(pos_matrix, dim=-1) + 0.000001)
        
        return loss.mean(), pos_dis.mean().item(), neg_dis.mean().item(), proportion_0_loss