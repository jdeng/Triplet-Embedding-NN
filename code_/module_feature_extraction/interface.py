import torch
import GPUtil
from torch.utils.tensorboard import SummaryWriter
import argparse
import sys


from .model import interface as M
from .data_helper import *
from .model_analysis import *
from .loss_function import *

from code_ import module_prediction as P

import warnings


warnings.filterwarnings("ignore", category=UserWarning)

if torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

model_path = './model_state/'

def set_parser():
    parser = argparse.ArgumentParser(description="-----[TenNet]-----")

    parser.add_argument("--model_name", default="")
    
    parser.add_argument("--test", default=False, action='store_true', help="if false, use all data")
    parser.add_argument("--job_num", default='abc',type=str, help="the job number on HPC")
    parser.add_argument("--log_directory", default="runs/", help="log directory of tensorboard")
    parser.add_argument("--dataset", default="COCO2017", choices=["NUS81", "NUS1k", "COCO2017", "artigo"], help="available datasets: NUS81, NUS1k, COCO2017")
    #parser.add_argument("--save_model", default=False, action='store_true', help="whether saving model or not")
    parser.add_argument("--seed", default=42, type=int, help="random seed")

    # training
    parser.add_argument("--epoch", default=0, type=int, help="number of max epoch")
    parser.add_argument("--batch_size", default=[40, 512], nargs="+", help="batch size")
    # learning rate
    parser.add_argument("--learning_rate", default=[0.00003, 0.00006], nargs="+", help="learning rate")
    parser.add_argument("--decay_model", default="step", choices=["step", "none"], help="learning rate decay model")
    parser.add_argument("--step_size", default=3, type=int, help="step size of step decay")
    parser.add_argument("--decay_rate", default=0.7, type=float, help="decay rate of step decay")
    
    # loss
    parser.add_argument("--loss_function", default="step", choices=["triplet", "contrastive","contrastive_one_margin", "cross_entropy", "step"], 
    help="loss function of model")
    parser.add_argument("--optimier", default="RMSprop", choices=["RMSprop", "Adadelta","Adagrad", "ASGD", "SGD"], 
    help="optimier")
    parser.add_argument("--weight_decay", default=0, type=float)

    # triplet loss
    parser.add_argument("--margin_distance", default=[0.2], nargs="+", help="margin distance between positive and negative samples")
    parser.add_argument("--self_adaption", default=False, action='store_true', help="whether self adapt margin distances")
    parser.add_argument("--loss_weight", default=[1,1,0.2,0.2], nargs="+", help="weights of IT, TI, II, TT losses")
    
    parser.add_argument("--batch_all", default=False, action='store_true')
    # hinge_IT
    parser.add_argument("--hinge_IT_loss_lambda", default=0, type=float, help="whether use IT hinge loss, and its weight")
    # step loss
    parser.add_argument("--step_hard", default=0, type=int)
    parser.add_argument("--step_top", default=0, type=int)
    parser.add_argument("--step_condition_type", default=0, type=int)
    parser.add_argument("--step_softplus", default=False, action='store_true')
    parser.add_argument("--step_margin_transform", default="linear", choices=["linear", "sq"], help="how to select a positive sample")
    # cross entropy
    parser.add_argument("--cross_entropy_replace", default="mean", choices=["mean", "0", "None"])
    
    # sampling
    parser.add_argument("--sampling_based_on_weight", default=False, action='store_true')
    parser.add_argument("--sampling_from_dataset", default=0, type=int)
    parser.add_argument("--sampling_single_tag", default=False, action='store_true')
    parser.add_argument("--sampling_subset", default=False, action='store_true')
    parser.add_argument("--sampling_2_tags", default=False, action='store_true')
    
    
    # image model
    parser.add_argument("--image_model", default="VGG16", choices=["VGG16", "ResNet18", "ResNet34", "ResNet50", "ResNet101", "EfficientNet_b3", "EfficientNet_b4", "Swin_b"], help="image model")
    parser.add_argument("--freeze_layers", default=0, type=int, help="whether freeze the first n layers of image model")
    
    # tag model
    parser.add_argument("--word_dimensions", default=256, type=int, help="the tag embedding dimensions")
    parser.add_argument("--tag_model", default="SCNN", choices=["SCNN", "Transformer", "Transformer_clip", "Embedding", "FC"], help="tag model")
    parser.add_argument("--tag_max_length", default=16, type=int, help="the max length of tags")
    # SCNN
    parser.add_argument("--filter_size_num_SCNN", default=3, type=int,  help="the number of filter size in SCNN")
    parser.add_argument("--filter_num_SCNN", default=100, type=int,  help="the number of filters for each size")
    parser.add_argument("--depth_of_SCNN", default=2, type=int, help="the number of convolutional layers in SCNN model")
    # Transformer nhead=8, d_hid=1024, nlayers=4
    parser.add_argument("--transformer_nhead", default=8, type=int, help="the number of head in transformer model")
    parser.add_argument("--transformer_nlayers", default=6, type=int, help="the number of self attention layers in transformer model")
    parser.add_argument("--transformer_d_hid", default=512, type=int, help="the number of features in hidden layer of transformer model")
    parser.add_argument("--sentence_feature", default="sum", choices=["eos", "sum", "mean", "max"], help="how to compute the sentence feature")
    parser.add_argument("--fc_layer", default=True, action='store_false', help="whether add a fully connected layer in transformer")
    
    # evaluation 
    parser.add_argument("--k_closest_tags", default=3, type=int, help="select k closest tags")
    # decoder
    parser.add_argument("--decoder_evaluate", default=False, action='store_true', help="whether train a decoder and use it to predict the tag set for images")
    parser.add_argument("--decoder_train", default=False, action='store_true', help="whether train the image model during (after) the training of decoder")
    parser.add_argument("--decoder_type", default="FC", choices=["FC", "PC", "PW", "Transformer"], help="use a normal, continuous or discrete decoder")
    parser.add_argument("--transformer_nloops", default=1, type=int)
    # decoder training
    parser.add_argument("--decoder_epoch", default=10, type=int, help="number of max epoch of decoder")
    parser.add_argument("--decoder_loss_type", default="BCE", choices=["Balanced_01", "BCE", "P1", "P2", 'Max', 'Exponential','huber'], help="decoder loss type")
    parser.add_argument("--decoder_learning_rate", default=0.0001, type=float, help="learning rate")
    parser.add_argument("--en_decoder_learning_rate", default=0.0001, type=float, help="learning rate")
    
    parser.add_argument("--parameter_number", default=False, action='store_true', help="print the parameter numbers of all models")
    
    #parser.add_argument("--gpu", default=-1, type=int, help="the number of gpu to be used")

    return parser


class extracter():
    def __init__(self) -> None:
        return

    def extract_features(self, s=[]):
            
        if len(s) != 0:
            sys.argv = s
        parser = set_parser()
        options = parser.parse_args()
        self.options = options
        
        options.batch_size = [int(i) for i in options.batch_size]
        print("batch size:", options.batch_size)
        options.loss_weight = [float(i) for i in options.loss_weight]
        print("loss weights:", options.loss_weight)

        # get name
        model_name = "bs=" + str(options.batch_size)

        # learning rate
        options.learning_rate = [float(i) for i in options.learning_rate]
        model_name = model_name + "_lr" + str(options.learning_rate[1])
        #model_name = model_name + "_dm" + str(options.decay_model)
        #model_name = model_name + "_dc" + str(options.decay_rate)
        model_name = model_name + "_sp" + str(options.step_size)
        
        # image model
        model_name = model_name + "_" + options.image_model[0:3] + options.image_model[-2:]
        Feature_Dimensions = 1000
        self.feature_dim = Feature_Dimensions

        # tag model
        model_name = model_name + "_" + options.tag_model[0:3]
        if options.tag_model == "SCNN":
            model_name = model_name + "-" + str(options.depth_of_SCNN)
        elif options.tag_model == "Transformer":
            model_name = model_name + "-" + str(options.transformer_nhead)
            model_name = model_name + "-" + str(options.transformer_nlayers)
            model_name = model_name + "-" + str(options.transformer_d_hid)
            model_name = model_name + "-" + options.sentence_feature
            if options.word_dimensions != Feature_Dimensions:
                options.fc_layer = True
            if options.fc_layer:
                model_name = model_name + "-" + "f"
        if "Embedding" not in options.tag_model:
            model_name = model_name + "_ml" + str(options.tag_max_length)
        else:
            model_name = model_name + options.tag_model[9:]
        model_name = model_name + "_wd" + str(options.word_dimensions)

        # sampling
                
        model_name = model_name + "_los=" + str(options.loss_function)[0:3]
        # step loss
        if options.loss_function == "step":
            model_name = model_name + "-h" + str(options.step_hard)
        if options.loss_function != "cross_entropy":
            options.margin_distance = [float(i) for i in options.margin_distance]
            model_name = model_name + "_md" + str(options.margin_distance[0])
            print("margin_distance", options.margin_distance)

        # decoder
        if options.decoder_evaluate:
            model_name = model_name + "_deE"
        if options.decoder_train:
            model_name = model_name + "_deT"
        if options.decoder_evaluate or  options.decoder_train:
            model_name = model_name + "_dl=" + str(options.decoder_loss_type)
            model_name = model_name + "_dt=" + str(options.decoder_type)[0:2]

        print("model_name:", model_name)

        with open('./job_info.txt', 'a') as f:
            f.write(options.job_num + ":" + model_name + "\n")

    # dataset
        if options.dataset == "NUS81": 
            train_data = nus_helper(nus_helper.DataSetType.Train_81, name="training", min_tag_num=1)
            test_data, valid_data = nus_helper.divide_into_test_valid(nus_helper.DataSetType.Test_81, test_p=0.5, min_tag_num=1, test=options.test)
        elif options.dataset == "NUS1k":
            train_data = nus_helper(nus_helper.DataSetType.Train_1k, name="training", min_tag_num=1)
            test_data, valid_data = nus_helper.divide_into_test_valid(nus_helper.DataSetType.Test_1k, test_p=0.5, min_tag_num=1, test=options.test)
        elif options.dataset == "COCO2017":
            train_data, valid_data, test_data = coco_helper.divide_into_train_valid_test(DataSetType.Train17, 
                                                        min_tag_num=1,train_p=0.9, test=options.test, seed=options.seed)
        elif options.dataset == "artigo":
            train_data, valid_data, test_data = artigo_helper.divide_into_train_valid_test(min_tag_num=1, train_p=0.9, test=options.test, seed=options.seed)
        valid_data.turn_off_data_augmentation() 
        test_data.turn_off_data_augmentation() 
        analysiz_data(train_data, options.k_closest_tags)

        num_workers = 3
        pin_memory = False
        train_loader = torch.utils.data.DataLoader(train_data, shuffle=True, drop_last=True, batch_size=options.batch_size[0], num_workers=num_workers, pin_memory=pin_memory)
        valid_loader = torch.utils.data.DataLoader(valid_data, shuffle=True, drop_last=True, batch_size=options.batch_size[0], num_workers=num_workers, pin_memory=pin_memory)
        test_loader = torch.utils.data.DataLoader(test_data, shuffle=True, drop_last=True, batch_size=options.batch_size[0], num_workers=num_workers, pin_memory=pin_memory)
        self.train_loader = train_loader
        self.valid_loader = valid_loader
        self.test_loader = test_loader
        #for_evaluate
        train_loader_E = torch.utils.data.DataLoader(train_data, batch_size=options.batch_size[0], num_workers=num_workers, pin_memory=pin_memory)
        valid_loader_E = torch.utils.data.DataLoader(valid_data, batch_size=options.batch_size[0], num_workers=num_workers, pin_memory=pin_memory) 
        test_loader_E = torch.utils.data.DataLoader(test_data, batch_size=options.batch_size[0], num_workers=num_workers, pin_memory=pin_memory) 
        self.train_loader_E = train_loader_E
        self.valid_loader_E = valid_loader_E
        self.test_loader_E = test_loader_E
    # log       
        log_path = options.log_directory + "/"  + options.dataset[0:3] + "/" + model_name + "/"
        writer = SummaryWriter(log_dir=log_path)

    # image model 
        image_model = Image_Model(options.image_model, options.freeze_layers).to(device)
        self.image_model = image_model
    # tag list
        tag_list = valid_data.get_tag_list()
        self.tag_num = len(tag_list)
        Word_Dimensions = options.word_dimensions 

    # word2vec
        # WORD_DIM 25 50 100 200
        # Word_Dimensions = 200
        word_embedding_matrix = compute_word_matrix("Word2Vec", train_data.image_tags, Word_Dimensions, 
                                                    end_symbol=(options.tag_model=="Transformer"))

    # tag model
        tag_model = Tag_Model(options, tag_list, word_embedding_matrix, feature_dim=Feature_Dimensions).to(device)
        self.tag_model = tag_model
        if options.parameter_number:
            output_parameter_numbers(options, tag_list, Feature_Dimensions, train_data.image_tags)

    # loss function
        # "triplet", "contrastive","contrastive_one_margin", "cross_entropy", "step"
        if options.loss_function == "triplet":
            loss_func = TripletLossFunc(options)
        elif options.loss_function == "cross_entropy":
            loss_func = CrossEntropyLossFunc(options)
        elif options.loss_function == "step":
            loss_func = StepMetricLossFunc(options)

    # optimizer
        if options.optimier == "RMSprop":
            optim = torch.optim.RMSprop
        elif options.optimier == "Adadelta":
            optim = torch.optim.Adadelta
        elif options.optimier == "Adagrad":
            optim = torch.optim.Adagrad
        elif options.optimier == "ASGD":
            optim = torch.optim.ASGD
        elif options.optimier == "SGD":
            optim = torch.optim.SGD
        optimizer = []
        if options.loss_function == "contrastive_one_margin":
            #filter(lambda p: p.requires_grad, image_model.parameters())
            optimizer = optim([{'params' : image_model.parameters()}, 
                                        {'params' : tag_model.parameters()},
                                        {'params' : loss_func.parameters()}], 
                                        lr=options.learning_rate)
        else:
            print()
            optimizer.append(optim([{'params' : image_model.parameters()}],  
                                        lr=options.learning_rate[0], weight_decay=options.weight_decay))
            optimizer.append(optim([{'params' : tag_model.parameters()}], 
                                        lr=options.learning_rate[1], weight_decay=options.weight_decay))

    # prediction decoder
        decoder = P.Decoder(options, Feature_Dimensions, len(tag_list), loop_n=options.transformer_nloops, tag_model=tag_model).to(device)
        self.decoder = decoder
        
        scheduler = []
        if options.decay_model == "step":
            scheduler.append(torch.optim.lr_scheduler.StepLR(optimizer[0], step_size=options.step_size, 
                                                        gamma=options.decay_rate))
            scheduler.append(torch.optim.lr_scheduler.StepLR(optimizer[1], step_size=options.step_size, 
                                                        gamma=options.decay_rate))
        else:
            scheduler = None

        #scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, gamma = gamma, last_epoch=-1)

    # run
        if options.epoch == 0 and len(options.model_name) != 0:
            try:
                checkpoint = torch.load(model_path + options.model_name)
                image_model.load_state_dict(checkpoint['image_model_state_dict'])   
                print("Load last checkpoint data")
            except FileNotFoundError:
                print("Can\'t found " + model_path + options.model_name)
                return False

        M.run(writer, image_model, tag_model, decoder, train_loader, valid_loader, train_loader_E, 
              valid_loader_E, test_loader_E, loss_func, optimizer, scheduler, options, print_log=True)
        if options.decoder_train or options.decoder_evaluate:
            P.run(options, options.decoder_train, writer, decoder, image_model, tag_model, train_loader, valid_loader, 
                train_loader_E, valid_loader_E, test_loader_E, n_epochs=options.decoder_epoch, index=-1)

        return image_model, tag_model

    def predict_k_closest(self, loader, k, number):
        P.predict(loader, self.image_model, self.decoder, self.tag_model, k=k, number=number)
        return

    def test_all_decoder(self):
        return
    
    def train_decoder(self, decoder_type, loss_type, n_epochs=20, name="", train_encoder=False, fc_layers=2, loop_n=1):
        options = self.options
        options.decoder_type = decoder_type
        options.decoder_loss_type = loss_type
        decoder = P.Decoder(options, self.feature_dim, self.tag_num, fc_layers, loop_n).to(device)
        log_path = options.log_directory + "/"  + options.dataset[0:3] + "/" + decoder_type + "_" + name + "/"
        writer = SummaryWriter(log_path)
        image_model = self.image_model
        tag_model = self.tag_model
        train_loader = self.train_loader
        valid_loader = self.valid_loader
        P.run(options, train_encoder, writer, decoder, image_model, tag_model, train_loader, valid_loader, n_epochs=n_epochs)
        return 
    
    def predict(self,number=2):
        train_loader = self.train_loader_E
        valid_loader = self.valid_loader_E
        test_loader = self.test_loader_E
        decoder = self.decoder
        image_model = self.image_model
        tag_model = self.tag_model
        print("prediction:")
        print("training dataset select 3 closest tags:")
        P.predict(train_loader, image_model, decoder, tag_model, k=3, number=number)
        print("validation dataset select 3 closest tags:")
        P.predict(valid_loader, image_model, decoder, tag_model, k=3, number=number)
        print("testing dataset select 3 closest tags:")
        P.predict(test_loader, image_model, decoder, tag_model, k=3, number=number)
        print("training dataset select undetermined number closest tags:")
        P.predict(train_loader, image_model, decoder, tag_model, k='decoder', number=number)
        print("validation dataset select undetermined number closest tags:")
        P.predict(valid_loader, image_model, decoder, tag_model, k='decoder', number=number)
        print("testing dataset select undetermined number closest tags:")
        P.predict(test_loader, image_model, decoder, tag_model, k='decoder', number=number)

    def test_all_kclosest(self, k=3):
        image_model = self.image_model 
        tag_model = self.tag_model
        train_loader_E = self.train_loader_E
        valid_loader_E = self.valid_loader_E
        test_loader_E = self.test_loader_E
        print("test: #####################################################################")
        print()

        train_loader_E.dataset.turn_off_data_augmentation()
        evaluation_res_train = P.evaluate_with_k_closest(train_loader_E, image_model, tag_model, k)
        evaluation_res_valid = P.evaluate_with_k_closest(valid_loader_E, image_model, tag_model, k)
        evaluation_res_test = P.evaluate_with_k_closest(test_loader_E, image_model, tag_model, k)
        P.print_evaluation("training", evaluation_res_train, "k closest")
        P.print_evaluation("validation", evaluation_res_valid, "k closest")
        P.print_evaluation("testing", evaluation_res_test, "k closest")
        train_loader_E.dataset.turn_on_data_augmentation()
        return

def get_tag_model(tag_list, tag_model="Transformer"):
    sys.argv = ["-f"]
    parser = set_parser()
    options = parser.parse_args()
    word_dim = 256
    options.tag_model = tag_model
    tag_num = len(tag_list)
    word_embedding_matrix = torch.zeros(tag_num+1, word_dim).to(device)
    tag_model = Tag_Model(options, tag_list, word_embedding_matrix, 1000).to(device)
    return tag_model

def load_models(image_model, tag_model, model_name):
    print(model_path + model_name)
    M.load_model(tag_model, image_model, model_path + model_name)
    
def get_images_tagsets_from_artigo(artigo_data, index_list):
    image_list = []
    tagset_list = []
    for index in index_list:
        image, tagset = artigo_data[index]
        image_list.append(image)
        tagset_list.append(tagset)
    
    return torch.stack(image_list).to(device), torch.stack(tagset_list).to(device)

def get_image_by_path(image_path):
    
    trans_to_image = transforms.ToPILImage()
    transform = transforms.Compose([transforms.Resize((IMAGE_WIDTH, IMAGE_HEIGHT)),
                                    transforms.ToTensor()])
    image = PIL.Image.open(image_path)
    if image.mode != "RGB":
        image = image.convert('RGB')
    image = transform(image)
    #display.display(trans_to_image(image))
    return image

def get_images_by_path(image_path_list):
    images = []
    for path in image_path_list:
        images.append(get_image_by_path(path))
    return torch.stack(images).to(device)

def get_tag_matrix(tag_name_matrix, tag_list):
    tag_num = len(tag_list)
    tag_matrix = torch.zeros((len(tag_name_matrix), tag_num))
    tag_name_index_dic = dict(zip(tag_list, range(0,tag_num)))
    for i in range(len(tag_name_matrix)):
        for tag in tag_name_matrix[i]:
            tag_matrix[i,tag_name_index_dic[tag]] = 1
    return tag_matrix.to(device)
 

if __name__ == "__main__":
    e = extracter()
    e.extract_features()