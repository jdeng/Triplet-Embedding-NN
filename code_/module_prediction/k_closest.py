import numpy as np
import torch
import torch.nn.functional as F
import random
from .Utils import *

from code_.module_prediction.evaluation_module import *

if torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')


def select_k_tags(loader, image_model, tag_model, k):
    k_tags = []
    similarity_matrix = []
    with torch.no_grad():
        single_tag_features = tag_model.compute_single_tag_feature()[0]
        for (x_images,y_tag_matrix) in loader:  
            x_images = x_images.to(device, non_blocking=True)   
            image_features = image_model(x_images)
            similarity =  torch.mm(image_features, single_tag_features.t())
            k_tags.append(torch.topk(similarity, k, largest=True)[1])
            similarity_matrix.append(similarity)
        k_tags = torch.cat(k_tags,dim=0)
        similarity_matrix = torch.cat(similarity_matrix,dim=0)
    return get_tag_vectors(k_tags, loader.dataset.tag_num) * (0.5 * similarity_matrix + 0.5)

def select_k_tags_one_by_one(loader, image_model, tag_model, k, rows=None, max_length=20):
    top_k_tags = []

    single_tag_features = tag_model.compute_single_tag_feature()[0]

    if rows == None:
        rows = range(loader.data.image_number)
    if k < 0:
        with torch.no_grad():
            for i in rows:
                image_i = loader.dataset.get_image(i)
                image_i = image_i.unsqueeze(0)
                image_i = image_i.to(device)
                image_features_i = image_model(image_i)
                
                dis = 1 - torch.mm(image_features_i, single_tag_features.t())
                top_k_tags_i = torch.topk(dis, max_length, largest=False)[1]
                top_k_tags_i = compute_undetermined_top_k(top_k_tags_i, tag_model, image_features_i, len(image_i), 
                                                    loader.dataset.tag_num, max_length)
                top_k_tags.append(top_k_tags_i)

            top_k_tags = torch.cat(top_k_tags,dim=0)
        return get_tag_vectors(top_k_tags, loader.dataset.tag_num+1)[:,0:-1], top_k_tags
    else:
        with torch.no_grad():
            for i in rows:
                image_i = loader.dataset.get_image(i)
                image_i = image_i.unsqueeze(0)
                image_i = image_i.to(device)
                image_features_i = image_model(image_i)
                
                dis = 1 - torch.mm(image_features_i, single_tag_features.t())
                top_k_tags_i = torch.topk(dis, k, largest=False)[1]
                top_k_tags.append(top_k_tags_i)

            top_k_tags = torch.cat(top_k_tags,dim=0)

        return get_tag_vectors(top_k_tags, loader.dataset.tag_num), None



def compute_undetermined_k_closest(loader, image_model, tag_model, max_length=20):
    res = []
    with torch.no_grad():
        single_tag_features = tag_model.compute_single_tag_feature()[0]

        for (x_images,y_tag_matrix) in loader:  
            x_images = x_images.to(device, non_blocking=True)   
            image_features = image_model(x_images)

            dis =  1 - torch.mm(image_features, single_tag_features.t())
            top_k = torch.topk(dis, max_length, largest=False)[1]
            
            res_i = compute_undetermined_top_k(top_k, tag_model, image_features, len(x_images), 
                                                    loader.dataset.tag_num, max_length)
            res.append(get_tag_vectors(res_i, loader.dataset.tag_num+1)[:,0:-1])
    return torch.cat(res)

def compute_undetermined_top_k(top_k, tag_model, image_features, bnum, tag_num, max_length):
    top_1_to_k_tag_matrix = []
    for i in range(max_length):
        top_i = get_tag_vectors(top_k[:,0:i+1], tag_num)
        top_1_to_k_tag_matrix.append(top_i)
    top_1_to_k_tag_matrix = torch.cat(top_1_to_k_tag_matrix, dim=1).view(-1, tag_num)

    tag_features = tag_model(top_1_to_k_tag_matrix)
    dis = 1 - torch.matmul(image_features.view(bnum,1,-1), tag_features.view(bnum, max_length, -1).transpose(1,2)).squeeze()
    num_list = torch.topk(dis, 1, largest=False)[1] + 1
    #num_list = torch.topk(dis, 1, largest=False)[1]*0 + 3
    #max_num = int(torch.max(num_list).item())
    #top_k = top_k[:,0:max_num]
    index_matrix_1 = num_list.view(-1,1).expand(-1,max_length)
    index_matrix_2 = torch.arange(0,max_length).to(device).view(1,-1).expand(len(index_matrix_1),-1)
    top_k = torch.where(index_matrix_2 < index_matrix_1, top_k, tag_num)
    return top_k

def evaluate_with_k_closest(loader, image_model, tag_model, k=3):
    image_model.eval()
    tag_model.eval()

    if k < 0:
        predicted_probability_matrix = compute_undetermined_k_closest(loader, image_model, tag_model)
    else:
        predicted_probability_matrix = select_k_tags(loader, image_model, tag_model, k)

    ground_truth_tag_matrix = torch.from_numpy(np.asarray(loader.dataset.image_tags)).to(device)

    return compute_evaluation_with_probability_matrix(predicted_probability_matrix, ground_truth_tag_matrix)

def predict_kc(loader, image_model, tag_model, k=3, number=20):
    data = loader.dataset
    image_model.eval()
    tag_model.eval()

    rows = random.sample(range(data.image_number), number)
    tag_matrix, top_k_tags = select_k_tags_one_by_one(loader, image_model, tag_model, k, rows)

    return data, tag_matrix, rows, top_k_tags