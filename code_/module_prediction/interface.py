import os
import torch
import random
import IPython.core.display as display 

from .decoder_model import *
from .k_closest import *
from .Utils import *
from .pos_closer import *
from .search import * 


def compute_loss(data, expect_res, encoder, decoder, loss_func):
    features = encoder(data)
    output = decoder(features)
    loss = loss_func(output, expect_res.float())  # 计算两者的误差
    return loss, output

def convert_one_1_to_0(tag_matrix, index_matrix, _1_nums):
    tag_num = len(tag_matrix[0])
    index_list = torch.arange(tag_num).view(1,-1).to(device)
    index_matrix = index_list.expand(tag_matrix.shape[0],-1)
    _1_nums = torch.sum(tag_matrix, dim=1)
    num = len(tag_matrix)
    index_matrix_1 = torch.where(tag_matrix == 1, index_matrix, tag_matrix-1).sort(descending=True)[0]
    position_y = torch.randint(0, torch.max(_1_nums+1), (num,)).to(device) %  _1_nums
    index_list = torch.gather(index_matrix_1, dim=1, index=position_y.view(-1,1)).view(-1,1)
    tag_matrix.scatter_(dim=1, index=index_list, value=0)
    return tag_matrix

def tag_dataset_generate(tag_matrix):
    input = []
    target = []
    inverse = 1 - tag_matrix
    index_list = torch.multinomial(torch.tensor([1]*len(tag_matrix[0])).float(), 7)
    for i in index_list:
        temp = tag_matrix.clone()
        temp[:,i] = inverse[:,i]
        input.append(temp)
        target.append(tag_matrix)
    input = torch.cat(input)
    target = torch.cat(target)
    return input, target

def single_epoch_computation(loss_func, decoder, image_model, tag_model, loader, optim, updata=True):
    # image, tag losses
    loss = [0,0]
    
    for index, (x_images,y_tags) in enumerate(loader):
        y_tags = y_tags.to(device)
        x_images = x_images.to(device)

        if decoder.decoder_type == "PW":
            single_emb = tag_model.compute_single_tag_feature()[0]
            decoder.set_single_embs(single_emb)

        tag_loss = torch.tensor(0).to(device)
        image_loss, output = compute_loss(x_images, y_tags, image_model, decoder, loss_func)
        batch_loss = image_loss

        if updata:
            optim.zero_grad()   # 清空上一步的残余更新参数值
            batch_loss.backward()         # 误差反向传播, 计算参数更新值
            optim.step()        # 将参数更新值施加到 net 的 parameters 上

        loss[0] = loss[0] + image_loss.item() * len(x_images)
        loss[1] = loss[1] + tag_loss.item() * len(x_images)
        
    num = loader.dataset.image_number
    loss[0] = loss[0] / num
    loss[1] = loss[1] / num

    return loss

def train(loss_func, decoder, image_model, tag_model, loader, optim, train_both):
    if train_both:
        image_model.train()
        tag_model.train()
    else:
        image_model.eval()
        tag_model.eval()
    decoder.train()
    res = single_epoch_computation(loss_func, decoder, image_model, tag_model, loader, optim, updata=True)

    return res

def validate(loss_func, decoder, image_model, tag_model, loader, optim):

    image_model.eval()
    decoder.eval()
    with torch.no_grad():
        res = single_epoch_computation(loss_func, decoder, image_model, tag_model, loader, optim, updata=False)
    return res 

def save_model(image_model, decoder, name):
    torch.save({
            'image_model_state_dict': image_model.state_dict(),
            'decoder_model_state_dict': decoder.state_dict(),
            }, name)

def load_model(image_model, decoder, name="../SavedModelState/decoder_model.ckpt"):
    try:
        print(name)
        checkpoint = torch.load(name)
        image_model.load_state_dict(checkpoint["image_model_state_dict"]) 
        decoder.load_state_dict(checkpoint["decoder_model_state_dict"]) 
        print("Load image model from" + name)
    except FileNotFoundError:
        print("Can\'t found " + name)
    return

def load_image_model(decoder, name="../SavedModelState/decoder_model.ckpt"):
    try:
        print(name)
        checkpoint = torch.load(name)
        decoder.load_state_dict(checkpoint["model_state_dict"]) 
        print("Load last checkpoint data")
    except FileNotFoundError:
        print("Can\'t found " + name)
    return
min_loss = 100
model_path = './model_state/'
def run(options, train_both, writer, decoder, image_model, tag_model, train_loader, valid_loader, 
        train_loader_E, valid_loader_E, test_loader_E, n_epochs=5, index=-1):
    
    if not train_both:
        optim = torch.optim.Adam(decoder.parameters(), lr= options.decoder_learning_rate)
        scheduler = torch.optim.lr_scheduler.StepLR(optim, step_size=1, gamma=0.3)
    else:
        #print("train_both")
        optim = torch.optim.Adam([{'params' : image_model.parameters()}, 
                                    {'params' : tag_model.parameters()},
                                    {'params' : decoder.parameters()}],  
                                    lr=options.en_decoder_learning_rate)
        scheduler = torch.optim.lr_scheduler.StepLR(optim, step_size=3, gamma=0.7)

    loss_func = DecoderLossFunc(options.decoder_loss_type)
    
    pbar = tqdm(range(n_epochs))
    for e in pbar:
        loss_train = train(loss_func, decoder, image_model, tag_model, train_loader, optim, train_both)
        loss_valid = validate(loss_func, decoder, image_model, tag_model, valid_loader, optim)  

        evaluation_train = evaluate_decoder(train_loader_E, image_model, decoder)
        evaluation_valid = evaluate_decoder(valid_loader_E, image_model, decoder)
        evaluation_test = evaluate_decoder(test_loader_E, image_model, decoder)

        if index != -1:
            print(f"encoder epoch {index}:")
        print(f"decoder epoch {e}: ########################################################")
        print()
        
        print("----1----  <training dataset> in train-model:")
        output_loss(loss_train) 
        print()
        print("----2----  <validation dataset> in evaluate-model")
        output_loss(loss_valid)
        print()

        decoder_type = decoder.decoder_type + "_" + options.decoder_loss_type + "_"
        print_evaluation(train_loader_E.dataset.name, evaluation_train, decoder_type) 
        print_evaluation(valid_loader_E.dataset.name, evaluation_valid, decoder_type)
        print_evaluation(test_loader_E.dataset.name, evaluation_test, decoder_type) 
        global min_loss
        print("min_loss:", min_loss, ",    loss:", loss_valid[0])
        if len(options.model_name) != 0 and (e == 0 or min_loss > loss_valid[0]):
            min_loss = loss_valid[0]
            print("save model")
            if not os.path.exists(model_path):
                os.makedirs(model_path)
            save_model(image_model, tag_model, model_path + options.model_name)

        if index == -1:
            writer.add_scalar(decoder_type + ' en_decoder_Loss/' + "train", loss_train[0], e)
            writer.add_scalar(decoder_type + ' en_decoder_Loss/' + "valid", loss_valid[0], e)
            write_evaluation_log(writer, evaluation_train, "train", e, decoder_type + "encoder_decoder_")
            write_evaluation_log(writer, evaluation_valid, "valid", e, decoder_type + "encoder_decoder_")
            scheduler.step()
        else:  
            writer.add_scalar(decoder_type + ' decoder_Loss/' + "train", loss_train[0], e)
            writer.add_scalar(decoder_type + ' decoder_Loss/' + "valid", loss_valid[0], e)
            write_evaluation_log(writer, evaluation_train, "train", e, decoder_type + "decoder_")
            write_evaluation_log(writer, evaluation_valid, "valid", e, decoder_type + "decoder_")
    return 
    
def evaluate_decoder(loader, encoder, decoder):
    predicted_probability_matrix = []
    ground_truth_tag_matrix = []
    with torch.no_grad():
        for x_images,y_tags in loader:
            y_tags = y_tags.to(device)
            x_images = x_images.to(device)
            features = encoder(x_images)
            output = decoder(features)
            predicted_probability_matrix.append(output)
            ground_truth_tag_matrix.append(y_tags)
        predicted_probability_matrix = torch.cat(predicted_probability_matrix, 0)
        ground_truth_tag_matrix = torch.cat(ground_truth_tag_matrix, 0)

    return compute_evaluation_with_probability_matrix(predicted_probability_matrix, ground_truth_tag_matrix)

def predict(loader, image_model, decoder, tag_model, number=20, k="decoder"):
    if k != None:
        data, tag_matrix, rows, top_k_tags = predict_kc(loader, image_model, tag_model, k=k, number=number)

    elif k == 'decoder':
        data = loader.dataset
        image_model.eval()
        if decoder == None:
            return
        decoder.eval()

        rows = random.sample(range(data.image_number), number)
        images = data.get_images(rows).to(device)
        features = image_model(images)
        if decoder.decoder_type == "PW":
            single_embedding_list, tags_list = tag_model.compute_single_tag_feature()
            output = decoder(features, single_embedding_list)
        else:
            output = decoder(features)

        tag_matrix = output >= 0.5
    elif k == "pos_closer":
        #data, tag_matrix, rows, top_k_tags  = predict_pos_closer(loader, image_model, tag_model, number=20)
        return

    for i, r in enumerate(rows):
        print("Prediction:")
        print_tags(data, tag_matrix[i])

        print("Ground Truth:")
        print_tags(data, data.image_tags[r])

        tp = (tag_matrix[i] * data.get_tags(r).to(device)).sum()
        print("True Positive: ", tp)

        #display(Image(data.get_image_path(data.get_image_name(r))))
        display.display(display.Image(data.get_image_path_by_index(r)))
            
