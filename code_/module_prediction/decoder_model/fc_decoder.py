import torch.nn as nn
import torch.nn.functional as F
from .Utils import *

class FCDecoder(nn.Module):
    def __init__(self, feature_dim, tag_num, dropout_probability=0.5):
        super().__init__()
        tag_number = tag_num
        self.decoder = nn.Sequential(
            
            nn.Linear(feature_dim, 2048),
            nn.BatchNorm1d(2048),
            nn.ReLU(),
            nn.Dropout(p=dropout_probability, inplace=False),

            nn.Linear(2048, 1024),
            nn.BatchNorm1d(1024),
            nn.ReLU(),
            nn.Dropout(p=dropout_probability, inplace=False),

            nn.Linear(1024, tag_number),
            nn.Sigmoid()
        )
 
    def forward(self, x):
        # decoder
        output = self.decoder(x)
        return output
