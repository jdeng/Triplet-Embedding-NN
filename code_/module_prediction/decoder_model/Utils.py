import torch
from code_.module_prediction.pos_closer import *

def compute_possibility_based_on_tag_model(image_features, tag_model):
    n = tag_model.tag_num
    single_tag_features, two_tags_features = tag_model.compute_2_tags_feature()
    sim_single = image_features @ (single_tag_features.t())
    sim_2 = torch.matmul(two_tags_features, image_features.t()).transpose(0,2)
    prediction = torch.sum((sim_2 - sim_single.view(-1,1,n)) > 0, dim=2)
    return  prediction / n 