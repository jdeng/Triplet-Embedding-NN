import torch.nn as nn

from .fc_decoder import *
from .pairwise_decoder import * 
from .transformer_decoder import *
from .pos_closer_decoder import *

class Decoder(nn.Module): 
    def __init__(self, options, feature_dim, tag_num, fc_layers=0, loop_n=1, tag_model=None):
        super().__init__()
        self.decoder_type = options.decoder_type

        if self.decoder_type == "FC":
            self.decoder = FCDecoder(feature_dim, tag_num)
        elif self.decoder_type == "PC":
            self.decoder = PCDecoder(tag_model)
        elif self.decoder_type == "PW":
            self.decoder = PWDecoder()
        elif self.decoder_type == "Transformer":
            self.decoder = TransformerDecoder(options, feature_dim, tag_num, fc_layers, loop_n, tag_model)
        
    def forward(self, x):
        output = self.decoder(x)
        return output