from ipaddress import summarize_address_range
import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np


if torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

class BasicConv2d(nn.Module):

    def __init__(self, in_channels, out_channels, **kwargs):
        super(BasicConv2d, self).__init__()
        self.conv = nn.Conv2d(in_channels, out_channels, **kwargs)
        self.bn = nn.BatchNorm2d(out_channels)

    def forward(self, x):
        x = self.conv(x)
        x = self.bn(x)
        return F.relu(x)

class GlobalMaxPool2d(nn.Module):
    def __init__(self):
        super().__init__()
    def forward(self, x):
        return F.max_pool2d(x, kernel_size=x.size()[2:])

class PWDecoder(nn.Module):
    def __init__(self):
        super().__init__()
        self.tag_num = 0
        self.tag_parameter_num = 12

        self.fc =   nn.Sequential(
                    nn.Linear(36 + self.tag_parameter_num, 128),
                    nn.ReLU(),
                    nn.Linear(128, 32),
                    nn.ReLU(),
                    nn.Linear(32, 1),
                    nn.Sigmoid()
                )

    def set_single_embs(self, single_embs):
        if self.tag_num == 0:
            self.tag_num = len(single_embs)
            self.tag_parameter = nn.Parameter(torch.tensor(np.ones([self.tag_num, self.tag_parameter_num])).float(), 
                                              requires_grad=True).to(device)
        self.single_embs = single_embs
    

    def compute_features(self, embedding, single_embedding_list):
        # shape tag_num x 3
        sum_msv  = self.get_msv(embedding + single_embedding_list)
        diff_msv = self.get_msv(embedding - single_embedding_list)
        mult_msv = self.get_msv(embedding * single_embedding_list)
        res_9 = torch.cat([sum_msv, diff_msv, mult_msv], dim=1)
        # shape 9 x 3 -> 27
        res_9_msv = self.get_msv(res_9.t()).view(-1)
        # shape tag_num x 27
        res_27 = torch.tensor(np.zeros((self.tag_num, 27))).to(device) + res_9_msv
        # shape tag_num x 36
        return torch.cat([res_9, res_27, self.tag_parameter], dim=1).float()
        
    # compute_max_sum_variance    
    def get_msv(self, features):
        dim = len(features.shape)-1
        res = [torch.max(features, dim=dim)[0], torch.sum(features, dim=dim), 
                torch.std(features, dim=dim)]
        return torch.stack(res, dim=dim)
 
    def forward(self, x):
        out = []
        for emb in x:
            out.append(self.fc(self.compute_features(emb, self.single_embs)).view(1, -1))
        out = torch.cat(out, dim=0)
        #print(out.shape)
        return out
