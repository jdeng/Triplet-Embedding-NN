import torch.nn as nn
import torch.nn.functional as F
from torch.nn import TransformerEncoder, TransformerEncoderLayer
from .Utils import *


class TransformerDecoder(nn.Module):

    def __init__(self, options, feature_dim, tag_num, fc_layers, loop_n, tag_model):
        super().__init__()
        self.loop_n = loop_n
        self.tag_model = tag_model
        #batch_first=True
        nhead = options.transformer_nhead
        self.d_hid = options.transformer_d_hid
        # d_model  the number of expected features in the encoder/decoder inputs
        self.token_embedding_dim = 1024 
        encoder_layers = TransformerEncoderLayer(self.token_embedding_dim, nhead, self.d_hid, batch_first=True)
        nlayers = options.transformer_nlayers
        self.transformer_encoder = TransformerEncoder(encoder_layers, nlayers)
        
        self.tag_num = tag_num
        self.feature_dim = feature_dim
        self.max_length = options.tag_max_length

        dropout_probability = 0.2
        if fc_layers == 1:
            self.fc = nn.Sequential(
                
                nn.Linear(self.feature_dim, 1024),
                nn.BatchNorm1d(1024),
                nn.ReLU(),
                nn.Dropout(p=dropout_probability, inplace=False),
                
                nn.Linear(1024, tag_num)
            )
        elif fc_layers == 2:
            self.fc = nn.Sequential(
                
                nn.Linear(self.feature_dim, 1024),
                nn.BatchNorm1d(1024),
                nn.ReLU(),
                nn.Dropout(p=dropout_probability, inplace=False),
                
                nn.Linear(1024, 1024),
                nn.BatchNorm1d(1024),
                nn.ReLU(),
                nn.Dropout(p=dropout_probability, inplace=False),

                nn.Linear(1024, tag_num)
            )
        elif fc_layers == 3:
            self.fc = nn.Sequential(
                
                nn.Linear(self.feature_dim, 1024),
                nn.BatchNorm1d(1024),
                nn.ReLU(),
                nn.Dropout(p=dropout_probability, inplace=False),
                
                nn.Linear(1024, 1024),
                nn.BatchNorm1d(1024),
                nn.ReLU(),
                nn.Dropout(p=dropout_probability, inplace=False),
                
                nn.Linear(1024, 1024),
                nn.BatchNorm1d(1024),
                nn.ReLU(),
                nn.Dropout(p=dropout_probability, inplace=False),

                nn.Linear(1024, tag_num)
            )

        self.tag_embedding_1 = nn.Embedding(self.tag_num, self.token_embedding_dim)
        self.tag_embedding_2 = nn.Embedding(self.tag_num, self.token_embedding_dim)

    def decode(self, input):
        token_embedding_matrix = self.tag_embedding_1.weight.unsqueeze(dim=0) * input.unsqueeze(dim=2)
        #print("1",fc_output.shape)
        output = self.transformer_encoder(token_embedding_matrix)
        #print("2",output.shape)
        output = output * self.tag_embedding_2.weight.unsqueeze(dim=0)
        #print("3",output.shape)
        output = torch.sum(output, dim=2)
        #print("4",output.shape)
        output.squeeze_()
        #print("5",output.shape)
        return output


    def forward(self, x):
        """
        Args:
            tag_matrix: Tensor, shape [batch_size, feature_dim]

        Returns:
            output Tensor of shape [batch_size, tag_num]
        """
        # [batch_size, feature_dim]  -> [batch_size, tag_num]
        output = compute_possibility_based_on_tag_model(x, self.tag_model)

        for i in range(self.loop_n):
            output = self.decode(output)

        return F.sigmoid(output)