import torch 
import torch.nn as nn 
import torch.nn.functional as F
from enum import IntEnum

class LossType (IntEnum):
    BCE = 0
    P1 = 1
    P2 = 2
    P3 = 3
    Pre_Rec = 4
    F1 = 5
    balanced_01_mean = 6
    max = 7
    Exponential = 8
    huber = 9

def myExpLoss (logits, labels):
    return  (( -( (2.0 * labels.float() - 1.0) * (2.0 * logits - 1.0) ) ).exp())

def myHuberLoss (output, labels):
    return F.huber_loss(output, labels, reduction='mean', delta=0.5) 

class DecoderLossFunc(nn.Module): 

    def __init__(self, loss_type): 
        super(DecoderLossFunc, self).__init__() 
        
        if  loss_type == "BCE":
            self.loss_type = LossType.BCE
        elif  loss_type == "P1":
            self.loss_type = LossType.P1
        elif  loss_type == "P2":
            self.loss_type = LossType.P2
        elif  loss_type == "P3":
            self.loss_type = LossType.P3
        elif  loss_type == "Pre_Rec":
            self.loss_type = LossType.Pre_Rec
        elif  loss_type == "F1":
            self.loss_type = LossType.F1
        elif  loss_type == "Balanced_01_mean":
            self.loss_type = LossType.balanced_01_mean
        elif  loss_type == "Max":
            self.loss_type = LossType.max
        elif  loss_type == "Exponential":
            self.loss_type = LossType.Exponential
        elif  loss_type == "huber":
            self.loss_type = LossType.huber
        else:
            print("wrong decoder loss type  ", loss_type)

    def forward(self, outputs, targets): 
        if self.loss_type == LossType.BCE:
            return F.binary_cross_entropy(outputs, targets)
        elif self.loss_type == LossType.P1:
            return torch.mean(F.pairwise_distance(outputs, targets, p=1))
        elif self.loss_type == LossType.P2:
            return torch.mean(F.pairwise_distance(outputs, targets, p=2))
        elif self.loss_type == LossType.P3:
            return torch.mean(F.pairwise_distance(outputs, targets, p=3))
        elif self.loss_type == LossType.balanced_01_mean:
            loss_1 = F.pairwise_distance(outputs * targets, targets, p=2)
            loss_0 = F.pairwise_distance(outputs  * (1-targets), targets-targets, p=2)
            loss_1_mean = torch.mean(loss_1)
            loss_0_mean = torch.mean(loss_0)
            return loss_1_mean + loss_0_mean
        elif self.loss_type == LossType.max:
            loss = torch.max((outputs - targets) * (outputs - targets), dim=1)[0]
            #loss = torch.max(torch.abs(outputs - targets), dim=1)
            return torch.mean(loss)
        elif self.loss_type == LossType.Exponential:
            #loss = torch.topk((outputs - targets) * (outputs - targets), k=int(len(outputs[0])*0.2+1), dim=1)[0]
            #print(loss.shape)
            loss = myExpLoss(outputs, targets)
            return torch.mean(loss)
        elif self.loss_type == LossType.huber:
            loss = myHuberLoss(outputs, targets)
            return torch.mean(loss)
        else:
            precisions = torch.sum(outputs * targets, dim=1) / (torch.sum(outputs, dim=1) + 0.000000001)
            recalls = torch.sum(outputs * targets, dim=1) / (torch.sum(targets, dim=1) + 0.000000001)
            if self.loss_type == LossType.Pre_Rec:
                return torch.mean(2 - precisions - recalls)
            elif self.loss_type == LossType.F1:
                F1 = precisions * recalls
                return torch.mean(1 - F1)
            else:
                print("wrong decoder loss type")
                return 

        