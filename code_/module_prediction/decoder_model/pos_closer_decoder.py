import torch.nn as nn
import torch.nn.functional as F
from .Utils import *

def compute_probability(image_features, single_tag_features, two_tags_features, tag_number): 
    n = tag_number
    sim_single = image_features @ (single_tag_features.t())
    sim_2 = torch.matmul(two_tags_features, image_features.t()).transpose(0,2)
    return (torch.sum((sim_2 - sim_single.view(-1,1,n)) > 0, dim=2) >= 0.5 * n).float()

class PCDecoder(nn.Module):
    def __init__(self, tag_model, dropout_probability=0.5):
        super().__init__()
        self.tag_model = tag_model
        self.single_tag_features, self.two_tags_features = tag_model.compute_2_tags_feature()
        self.tag_number = tag_model.tag_num
        self.decoder = nn.Sequential(
            
            nn.Linear(self.tag_number, 1024),
            nn.BatchNorm1d(1024),
            nn.ReLU(),
            nn.Dropout(p=dropout_probability, inplace=False),

            nn.Linear(1024, 1024),
            nn.BatchNorm1d(1024),
            nn.ReLU(),
            nn.Dropout(p=dropout_probability, inplace=False),

            nn.Linear(1024, self.tag_number),
            nn.Sigmoid()
        )
 
    def forward(self, x):
        # decoder
        output = compute_probability(x, self.single_tag_features, self.two_tags_features, self.tag_number)
        #print(output)
        output = self.decoder(output)
        return output
