import torch
import numpy as np

def compute_mAP(predicted_probability_matrix, ground_truth_tag_matrix):
    ap = []
    for i in range(len(predicted_probability_matrix[0])):
        probability_i = predicted_probability_matrix[:,i]
        probability_i, indices = torch.sort(probability_i, descending=True)
        target_i = ground_truth_tag_matrix[:,i]
        target_i = target_i[indices]

        pre, n = 0, 0
        for j in range(len(predicted_probability_matrix)):
            if target_i[j] == 1:
                n += 1.0
                pre += n / (j+1)
        if n == 0:
            ap.append(0)
        else:
            ap.append(pre/n)
    return sum(ap)/len(ap)

def compute_evaluation_with_probability_matrix(predicted_probability_matrix, ground_truth_tag_matrix):
    with torch.no_grad():
        predicted_tag_matrix = predicted_probability_matrix > 0.5

        macro_evaluation, micro_evaluation = compute_evaluation_with_tag_matrix(predicted_tag_matrix, ground_truth_tag_matrix)
        mAP = compute_mAP(predicted_probability_matrix, ground_truth_tag_matrix)
    return macro_evaluation, micro_evaluation, mAP

# compute evaluation for a dataset
def compute_evaluation_with_tag_matrix(predicted_tag_matrix, ground_truth_tag_matrix):
    with torch.no_grad():
        tag_num = len(predicted_tag_matrix[0])
        image_num = len(predicted_tag_matrix)
        #print("image_num", image_num)
        # 80
        tag_tp = torch.sum(predicted_tag_matrix * ground_truth_tag_matrix, dim=0).float() 
        #print("tag_tp",tag_tp)
        tag_sum_p = torch.sum(predicted_tag_matrix, dim=0).float() 
        #print("tag_sum_p",tag_sum_p)
        tag_sum_g = torch.sum(ground_truth_tag_matrix, dim=0).float() 
        #print("tag_sum_g",tag_sum_g)
        tag_fp = tag_sum_p - tag_tp
        #print("tag_fp",tag_fp)
        tag_fn = tag_sum_g - tag_tp 
        #print("tag_fn",tag_fn)
        tag_tn = image_num -  tag_fp - tag_fn - tag_tp 
        #print("tag_tn",tag_tn)
 
        micro_evaluation = compute_single_evaluation_terms(tag_tp.sum().item(), tag_tn.sum().item(), tag_fp.sum().item(), tag_fn.sum().item(), image_num)
        
        macro_evaluation = np.asarray([0] * 8)
        for i in range(tag_num):
            single_evaluation = compute_single_evaluation_terms(tag_tp[i].item(), tag_tn[i].item(), tag_fp[i].item(), tag_fn[i].item(), image_num)
            macro_evaluation = macro_evaluation + single_evaluation

        macro_evaluation = macro_evaluation / tag_num
    return macro_evaluation, micro_evaluation

# compute evaluation for a image
def compute_single_evaluation_terms(tp, tn, fp, fn, num):

    #print("#####:",tp, tn, fp, fn)
    sum_p = tp + fp
    sum_g = tp + fn

    if sum_p == 0:
        precision = 0
    else:
        precision = tp / sum_p

    if sum_g == 0:
        recall = 0
    else:
        recall = tp / sum_g
        
    if precision == 0 and recall == 0:
        f1 = 0
    else:
        f1 = 2 * precision * recall / (precision + recall)
    accuracy = (tp+tn) / (tp+tn+fp+fn)
    return np.asarray([precision, recall, f1, accuracy, tp/num, tn/num, sum_p/num, sum_g/num])

def print_evaluation(dataset_name, evaluation, predictor_name):
    macro_precision = evaluation[0][0]
    macro_recall = evaluation[0][1]
    macro_F1 = evaluation[0][2] 
    macro_accuracy = evaluation[0][3]

    micro_precision = evaluation[1][0]
    micro_recall = evaluation[1][1]
    micro_F1 = evaluation[1][2]
    micro_accuracy = evaluation[1][3]
    if len(evaluation) == 3:
        mAP = evaluation[2]

    print( "Evaluate result of method \"" + predictor_name + "\" on " + dataset_name + " dataset:")
    print(  f"    macro_Precision: {macro_precision:.4f},  " +
            f"macro_Recall: {macro_recall:.4f},  " +
            f"macro_F1: {macro_F1:.4f},  " + 
            f"macro_Accuracy: {macro_accuracy:.4f}." )
    print(  f"    micro_Precision: {micro_precision:.4f},  " +
            f"micro_Recall: {micro_recall:.4f},  " +
            f"micro_F1: {micro_F1:.4f},  " + 
            f"micro_Accuracy: {micro_accuracy:.4f}." )
    print(  f"    True_positive: {evaluation[1][4]:.4f},  " +
            f"prediction_tags: {evaluation[1][6]:.4f},  " +
            f"ground_truth_tags: {evaluation[1][7]:.4f}." )
    if len(evaluation) == 3:
        print(  f"    mAP: {mAP:.4f}" )
    print()

def write_evaluation_log(writer, res, name, index, predictor_name=""):
    #print(len(res), len(res[0]))
    #print(res)
    # [precision, recall, F1, accuracy], [], tp, tn, sum_p, sum_g
    writer.add_scalar(predictor_name + 'macro_precision/' + name, res[0][0], index)
    writer.add_scalar(predictor_name + 'macro_recall/'  + name, res[0][1], index)
    writer.add_scalar(predictor_name + 'macro_F1/' + name, res[0][2], index)
    #writer.add_scalar(predictor_name + 'macro_accuracy/' + name, res[0][3], index)

    writer.add_scalar(predictor_name + 'micro_precision/' + name, res[1][0], index)
    writer.add_scalar(predictor_name + 'micro_recall/'  + name, res[1][1], index)
    writer.add_scalar(predictor_name + 'micro_F1/' + name, res[1][2], index)
    #writer.add_scalar(predictor_name + 'micro_accuracy/' + name, res[1][3], index)
 