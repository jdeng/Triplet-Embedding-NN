import numpy as np
import torch
import torch.nn.functional as F
import random
from .Utils import *

from code_.module_prediction.evaluation_module import *



def compare_1_2(loader, image_model, tag_model):
    n = tag_model.tag_num
    predicted_probability_matrix = []
    with torch.no_grad():
        single_tag_features, two_tags_features = tag_model.compute_2_tags_feature()
        for (x_images,y_tag_matrix) in loader:  
            x_images = x_images.to(device, non_blocking=True)   
            image_features = image_model(x_images)
            sim_single = image_features @ (single_tag_features.t())
            sim_2 = torch.matmul(two_tags_features, image_features.t()).transpose(0,2)
            probability_i = torch.mean((sim_2 - sim_single.view(-1,1,n)), dim=2)/4 + 0.5
            predicted_probability_matrix.append(probability_i)

    return  torch.cat(predicted_probability_matrix,dim=0)

def evaluate_with_pos_closer(loader, image_model, tag_model):
    image_model.eval()
    tag_model.eval()

    predicted_probability_matrix = compare_1_2(loader, image_model, tag_model)

    ground_truth_tag_matrix = torch.from_numpy(np.asarray(loader.dataset.image_tags)).to(device)

    return compute_evaluation_with_probability_matrix(predicted_probability_matrix, ground_truth_tag_matrix)