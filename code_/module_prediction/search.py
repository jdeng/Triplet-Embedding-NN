import numpy as np
import torch
import torch.nn.functional as F
import random
from .Utils import *

from code_.module_prediction.evaluation_module import *

def compute_image_features(data, image_model):
    image_model.eval()
    with torch.no_grad():
        loader = torch.utils.data.DataLoader(data, batch_size=96)
        image_features = []
        tag_matrix = []
        for (x_images,y_tag_matrix) in loader:  
            x_images = x_images.to(device)
            y_tag_matrix = y_tag_matrix.to(device)
            image_features_i = image_model(x_images)
            image_features.append(image_features_i)
            tag_matrix.append(y_tag_matrix)
    return torch.cat(image_features), torch.cat(tag_matrix)

def clip_tag_vector(tag_vector, max_length):
    indices = (tag_vector == 1).nonzero()
    indices = indices[random.sample(range(len(indices)), max_length)]
    tag_vector.zero_()
    tag_vector[indices] = 1
    return tag_vector


def clip_tag_matrix(tag_matrix, max_length):
    tag_matrix = torch.unique(tag_matrix, dim=0)
    index = torch.sum(tag_matrix, dim=1) <= max_length
    tag_matrix_too_long = tag_matrix[~index]
    tag_matrix = tag_matrix[index]
    cliped_tag_vecter = []
    for tag_vector in tag_matrix_too_long:
        for i in range(int(torch.sum(tag_vector)-max_length) * 4):
            cliped_tag_vecter.append(clip_tag_vector(tag_vector, max_length))

    if len(cliped_tag_vecter) == 0:
        return tag_matrix
    return torch.cat([tag_matrix, torch.stack(cliped_tag_vecter)])

def compute_tag_features(tag_model, tag_matrix, max_length):
    tag_model.eval()
    with torch.no_grad():
        tag_features = []
        tag_matrix = clip_tag_matrix(tag_matrix, max_length)
        num = len(tag_matrix)
        bs = 512
        for i in range(int(num/bs)):
            tag_features_i = tag_model(tag_matrix[i*bs:min(num, i*bs + bs)])
            tag_features.append(tag_features_i)

    return  torch.cat(tag_features), tag_matrix

def evaluate_with_search(loader, image_model, tag_model):
    image_model.eval()
    tag_model.eval()
    data = loader.dataset

    image_features, ground_truth_tag_matrix = compute_image_features(data, image_model)
    tag_features, tag_matrix = compute_tag_features(tag_model, ground_truth_tag_matrix, tag_model.max_length)
    
    predicted_probability_matrix = []
    for image_feature in image_features:
        similarity_i = (tag_features @ image_feature.view(-1,1)).squeeze()
        value, index = torch.topk(similarity_i, 1)
        predicted_probability_matrix.append(tag_matrix[index] * (0.5 * value + 0.5))
    predicted_probability_matrix = torch.cat(predicted_probability_matrix)
    #print(predicted_tag_matrix.shape)
    return compute_evaluation_with_probability_matrix(predicted_probability_matrix, ground_truth_tag_matrix)