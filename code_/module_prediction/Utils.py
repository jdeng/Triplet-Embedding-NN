import torch
from jupyterplot import ProgressPlot
from tqdm.notebook import tqdm
import math

from code_.module_prediction.evaluation_module import *

if torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')


def get_tag_vectors(indexes_list, n):
    return torch.zeros(len(indexes_list),n).to(device).scatter_(1, indexes_list, 1)

def compute_column_maximum(m):
    res = [i for i in m[0][0]]
    for i in range(len(m)):
        for j in range(len(m[0][0])):
            if m[i][0][j] > res[j]:
                res[j] = m[i][0][j]
            if m[i][1][j] > res[j]:
                res[j] = m[i][1][j]
    return res

def print_tags(data, tag_v, top_k_tags=None):
    if  top_k_tags == None:
        tags = [data.tag_list[i] for i in range(len(tag_v)) if tag_v[i] ]
    else:
        tags = [data.tag_list[top_k_tags[i]] for i in range(torch.sum(tag_v))]
    print(tags)

def output_loss(loss):
    print(f"    image_loss: {loss[0]:.6f},  tag_loss: {loss[1]:.6f}")
    return

def printLossLog(res, n_epochs):

    pbar = tqdm(range(min(len(res), n_epochs)))
    for e in pbar:
        
        loss_num_train =  res[e][0]
        output_loss(f"epoch:{e}: 1-train dataset with train model", loss_num_train)
        
        loss_num_valid = res[e][1]   
        output_loss(f"epoch:{e}: 2-valid dataset with evaluate model", loss_num_valid)

def printLossProgressPlot(res, n_epochs):

    pp = ProgressPlot(plot_names=["loss"],
                    line_names=["train", "valid"],
                    x_lim=[0, n_epochs-1], 
                    y_lim=[0, res[0][0]])

    pbar = tqdm(range(min(len(res), n_epochs)))
    for e in pbar:
    
        train_loss = res[e][0]
        valid_loss = res[e][1]
    
        pp.update([[train_loss[0], valid_loss[0]]])

    pp.finalize()


   