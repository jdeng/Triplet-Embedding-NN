import torch
from code_.module_feature_extraction import *

import matplotlib.pylab as plt
import matplotlib as mpl

if torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

class searcher():
    def get_transformer_tag_model(self):
        sys.argv = ["-f"]
        parser = set_parser()
        options = parser.parse_args()
        word_dim = 256
        options.tag_model = self.options.tag_model
        
        word_embedding_matrix = torch.zeros(self.tag_num+1, word_dim).to(device)
        tag_model = Tag_Model(options, self.tag_list, word_embedding_matrix, 1000).to(device)
        return tag_model

    def load_models(self):
        #print(model_path + self.options.model_name)
        M.getModel(self.tag_model, self.image_model, model_path + self.options.model_name)
   

    def __init__(self, s=[]):

        parser = argparse.ArgumentParser(description="-----[]-----")

        parser.add_argument("--model_name", default="model", type=str)
        parser.add_argument("--base_dataset", default="artigo", type=str)
        parser.add_argument("--image_dataset", default="artigo", type=str)
        parser.add_argument("--tag_model", default="Transformer", choices=["SCNN", "Transformer", "Transformer_clip", "Embedding", "FC"], help="tag model")
        
        if len(s) != 0:
            sys.argv = s
        options = parser.parse_args()
        self.options = options
        print("options.model_name", options.model_name)

        if options.base_dataset == "artigo":
            base_dataset = artigo_helper("all", min_tag_num=0)
        elif options.base_dataset == "coco":
            base_dataset = coco_helper(DataSetType.Train17, "all", min_tag_num=1)

        base_dataset.turn_off_data_augmentation()
        print(base_dataset.print_tag_frequence())
        print("tag number: ",base_dataset.tag_num)
        self.tag_list = base_dataset.tag_list
        self.tag_num = len(self.tag_list)
        
        self.image_model = Image_Model("ResNet50").to(device)
        self.tag_model = self.get_transformer_tag_model()
        self.load_models()

        # compute feature for all images
        if options.image_dataset == options.base_dataset:
            self.data = base_dataset
        elif options.image_dataset ==  'imagesSmall':
            self.data = image_helper(path='/clusterarchive/MET_data/imagesSmall')
        else:
            print('wrong image set')
            return

        feature_list = self.compute_features()
        self.image_features = feature_list

    def compute_features(self):
        self.image_model.eval()
        with torch.no_grad():
            self.image_num = self.data.image_number
            loader = torch.utils.data.DataLoader(self.data, batch_size=96)
            res = []
            tag_matrix = []
            for (x_images,y_tag_matrix) in loader:  
                x_images, y_tag_matrix = x_images.to(device), y_tag_matrix.to(device) 
                image_features = self.image_model(x_images)
                res.append(image_features)
                tag_matrix.append(y_tag_matrix)
            self.tag_matrix = torch.cat(tag_matrix)
        return torch.cat(res)

    def search_based_on_tag_set(self, tag_name_set, k=3):
        self.tag_model.eval()
        tag_matrix = torch.zeros((1, self.tag_num))
        tag_name_index_dic = dict(zip(self.tag_list, range(0,self.tag_num)))
        for tag in tag_name_set:
            tag_matrix[0,tag_name_index_dic[tag]] = 1
        tag_matrix = tag_matrix.to(device)
        tag_feature = self.tag_model(tag_matrix)
        cosione_similarity = (self.image_features @ tag_feature.T).squeeze()
        index_list = torch.topk(cosione_similarity, k=k)[1]
        for index in index_list:
            if self.options.image_dataset != 'imagesSmall':
                print_tags(self.data, self.data.image_tags[index])
            display.display(display.Image(self.data.get_image_path_by_index(index)))

        return 

    def search_based_on_new_tag_set(self, tag_name_set, k=3):
        self.tag_model.eval()
        tag_feature = self.tag_model(tag_sets=tag_name_set)
        cosione_similarity = (self.image_features @ tag_feature.T).squeeze()
        index_list = torch.topk(cosione_similarity, k=k)[1]
        for index in index_list:
            if self.options.image_dataset != 'imagesSmall':
                print_tags(self.data, self.data.image_tags[index])
            display.display(display.Image(self.data.get_image_path_by_index(index)))

        return 
   
if __name__ == "__main__":
    searcher().search()