from .base import *


class map_to_low_dim_alg (base):

    def __init__(self, recommend_based_on_possibility=True):
        super().__init__(recommend_based_on_possibility)
        self.name = "map_to_low_dim_alg"

    def set_first_element(self, features, first_index):
        self.dims = len(features[0])
        self.map_matrix = torch.eye(self.dims)
        self.features = features
        self.similarity = features @ features[first_index]
        self.similarity[first_index] = visited_state
        self.cluster_features = [features[first_index]]
        self.priority = self.similarity
        return

    def update(self, new_image_index, is_in_cluster):
        if is_in_cluster:
            with torch.no_grad():
                self.cluster_features.append(self.features[new_image_index])
                self.compute_map()
                self.similarity = self.compute_similarity(self.features @ self.map_matrix, self.target_feature)
        with torch.no_grad():
            self.priority[new_image_index] = visited_state
            condition = self.priority == visited_state
            self.priority = torch.where(condition, self.priority, self.similarity)
        return

    def compute_map(self):
        n = min(len(self.cluster_features), len(self.cluster_features[0]))
        fm = torch.stack(self.cluster_features[0:n])
        #if torch.det(fm[:, 0:n]) == 0:
        if torch.any(torch.svd(fm[:, 0:n], compute_uv=False)[1] < torch.finfo(fm[:, 0:n].dtype).eps) :
            #print("torch.det(fm[:, 0:n])", torch.det(fm[:, 0:n]))
            #print("singular matrix, delete the last feature, n=", n)
            self.cluster_features.pop()
            return
        A = fm[:,0:n]
        B = fm[:,n:] + torch.ones((1,1)).to(device)
        #print(torch.ones((n, 1)).to(device).shape, B.shape)
        B = torch.cat([torch.ones((n, 1)).to(device), B], dim=1)
        X = torch.solve(B, A)[0]
        #print("X")
        #print(X)
        temp = torch.cat([torch.zeros((self.dims-n, 1)).to(device), torch.eye(self.dims-n).to(device)], dim=1)
        self.map_matrix = torch.cat([X, temp])
        self.target_feature = torch.mean(fm @ self.map_matrix, dim=0)
        #print(torch.sum(fm @ self.map_matrix, dim=1))
        #print("aaaaa")
        #print(self.compute_distance(fm@ self.map_matrix, self.target_feature)[0:10])
        #print("bbbbb")
        #print(self.compute_distance(self.features[0:10] @ self.map_matrix, self.target_feature))

    def compute_similarity(self, m1, m2):
        similarity = m1 @ m2
        return self.min_max_normalization(similarity)

    def compute_distance(self, m1, m2):
        distance = torch.sum(m1 * m1, dim=1) + torch.sum(m2 * m2) - 2 * m1 @ m2
        return distance
    
    def min_max_normalization(self, v):
        v_min, v_max = v.min(), v.max()
        if v_min == v_max:
            return v - v + 1
        return (v - v_min)/(v_max - v_min)
        #new_min, new_max = 0, 1
        #return (v - v_min)/(v_max - v_min)*(new_max - new_min) + new_min


