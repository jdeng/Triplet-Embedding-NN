from .base import *

class mean_dis_alg (base):

    def __init__(self, recommend_based_on_possibility=True):
        super().__init__(recommend_based_on_possibility) 
        self.name = "mean_dis_alg"

    def set_first_element(self, features, first_index):
        self.features = features
        self.pos_simlarity = features @ features[first_index]
        self.pos_simlarity[first_index] = visited_state
        self.priority = self.pos_simlarity
        return

    def update(self, new_image_index, is_in_cluster):
        if is_in_cluster:
            with torch.no_grad():
                self.pos_simlarity = self.pos_simlarity + self.features @ self.features[new_image_index]
        with torch.no_grad():
            self.priority[new_image_index] = visited_state
            condition = self.priority == visited_state
            self.priority = torch.where(condition, self.priority, self.pos_simlarity)
        return 