from .base import *

class mean_pos_neg_dis_alg (base):


    def __init__(self, recommend_based_on_possibility=True):
        super().__init__(recommend_based_on_possibility) 
        self.name = "mean_pos_neg_dis_alg"
        self.pos_n = 0
        self.neg_n = 0

    def set_first_element(self, features, first_index):
        self.features = features
        self.pos_simlarity = features @ features[first_index]
        self.pos_simlarity_sum = self.pos_simlarity
        self.neg_simlarity = self.pos_simlarity * 0
        self.neg_simlarity_sum = self.neg_simlarity
        self.pos_simlarity[first_index] = visited_state
        self.priority = self.pos_simlarity
        return
        
    def update(self, new_image_index, is_in_cluster):
        with torch.no_grad():
            if is_in_cluster:
                self.pos_n += 1
                self.pos_simlarity_sum = self.pos_simlarity + self.features @ self.features[new_image_index]
                self.pos_simlarity = self.pos_simlarity_sum / self.pos_n
            else:
                self.neg_n += 1
                self.neg_simlarity_sum = self.neg_simlarity + self.features @ self.features[new_image_index] 
                self.neg_simlarity = self.neg_simlarity_sum / self.neg_n
                
            self.priority[new_image_index] = visited_state
            self.compute_priority_with_pos_neg()
        return