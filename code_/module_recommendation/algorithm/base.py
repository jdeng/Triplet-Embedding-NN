import torch

if torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

visited_state = -100000000

class base ():

    def __init__(self, recommend_based_on_possibility=True):
        self.recommend_based_on_possibility = recommend_based_on_possibility
        return

    def set_first_element(self):
        return

    def get_recommendation(self):
        if self.recommend_based_on_possibility:
            return self.recommend_by_value(self.priority)
        else:
            return torch.topk(self.priority, 1)[1][0]

    def update(self, new_image_index):
        return

    def set_visited(self, new_image_index):
        self.priority[new_image_index] = visited_state
        return
    
    def compute_priority_with_pos_neg(self):
        if True:
            condition = self.priority == visited_state
            self.priority = torch.where(condition, self.priority, self.pos_simlarity + (1-self.neg_simlarity)*3)
            return 
        condition = self.priority == visited_state
        self.priority = torch.where(condition, self.priority, self.pos_simlarity)
        a = max(self.priority.topk(100)[0][-1], -1)
        condition = self.priority >= a
        self.priority = torch.where(condition, self.priority, self.pos_simlarity + (1-self.neg_simlarity)*0.5)


    def recommend_by_value(self, priority_list):
        priority_list, index_list = torch.topk(priority_list, int(len(priority_list)*0.05+1))
        v_min, v_max = priority_list.min(), priority_list.max()
        if v_max == v_min:
            normalized_list = (priority_list - v_min)  + 1
        else:
            normalized_list = (priority_list - v_min) / (v_max - v_min)
        probability_list = normalized_list / normalized_list.sum()
        index = torch.multinomial(probability_list, 1)
        #print("test:","1:",priority_list[0],", -1:", priority_list[-1], ", selected:", priority_list[index])
        return index_list[index]





