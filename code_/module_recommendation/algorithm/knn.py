from .base import *

class knn_alg (base):

    def __init__(self, k=5, recommend_based_on_possibility=True):
        super().__init__(recommend_based_on_possibility) 
        self.name = "knn_alg_k=" + str(k)
        self.k = k

 
    def compute_knn_graph(self):
        batch_size = 100
        graph = []
        p = 0
        while p < len(self.features):
            np = min(p+batch_size,len(self.features))
            graph.append(torch.topk(self.features[p:np] @ self.features.t(), k=self.k, dim=1)[1])
            p = np
        self.graph = torch.cat(graph)
        return

    def set_first_element(self, features, first_index):
        self.features = features
        self.compute_knn_graph()
        self.priority = torch.zeros(len(self.features)).to(device)
        self.priority[first_index] = visited_state
        return

    def update(self, new_image_index, is_in_cluster):
        state = torch.sum(self.graph==new_image_index, dim=1)
        if is_in_cluster:
            self.priority = self.priority + state
        else:
            self.priority = self.priority - state
        with torch.no_grad():
            self.priority[new_image_index] = visited_state
        return