import torch
import argparse
import sys
from code_.module_feature_extraction import *
from code_.module_evaluation import *
from .algorithm import *

import matplotlib.pylab as plt
import matplotlib as mpl

if torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')
model_path = './model_state/'

class recommender():
    
    base_evaluater_mean_dis = None
    base_evaluater_min_pos_neg = None
    evaluater_mean_dis = None
    evaluater_mean_pos_neg_dis = None
    evaluater_min_dis = None
    evaluater_map_to_low_dim = None
    evaluater_min_pos_neg_dis = None
    evaluater_knn = None
    image_num = -1

    def __init__(self):
        self.evaluater_list = []
        self.basic_evaluater_list = []
        self.artigo_helper = None
        self.artigo_helper = artigo_helper("all", min_tag_num=0)
        print(self.artigo_helper.get_tag_matrix_based_on_tagset(['bäume', 'landschaft']).dtype)
        return

    def getModel(self, image_model, name):
        print(name)
        try: 
            checkpoint = torch.load(name)
            image_model.load_state_dict(checkpoint['image_model_state_dict'])   
            print("Load image model from" + name)
            return True
        except FileNotFoundError:
            print("Can\'t found " + name)
            return False

    def recommend(self, s=[]):

        parser = argparse.ArgumentParser(description="-----[]-----")

        parser.add_argument("--model_name", default="model", type=str)
        parser.add_argument("--epoch", default=0, type=int)
        parser.add_argument("--all", default=False, action='store_true')
        parser.add_argument("--knn", default=0, type=int)
        parser.add_argument("--mean_distance", default=False, action='store_true')
        parser.add_argument("--mean_pos_neg_dis", default=False, action='store_true')
        parser.add_argument("--min_distance", default=False, action='store_true')
        parser.add_argument("--map_to_low_dim", default=False, action='store_true')
        parser.add_argument("--min_pos_neg_dis", default=False, action='store_true')
        parser.add_argument("--top1", default=False, action='store_true')
        parser.add_argument("--baseline", default=False, action='store_true')
        parser.add_argument("--md", default='0.1', type=str)

        
        if len(s) != 0:
            sys.argv = s
        self.options = parser.parse_args()
        print("options.model_name", self.options.model_name)
        
        self.e = extracter()
        image_model = self.e.extract_features(s=["-f", "--job_num=abcd",
            "--epoch=" + str(self.options.epoch),
            "--batch_size=32",
            "--loss_function=step", 
            "--tag_max_length=28", 
            "--k_closest_tags=20",
            "--dataset=artigo", 
            "--image_model=ResNet101", "--tag_model=Transformer"])[0] 
        self.getModel(image_model, model_path + self.options.model_name)
        if self.options.baseline:
            self.compute_baseline()
        # compute feature for all images
        feature_list = self.compute_features(image_model)
        self.image_model = image_model
        self.features = feature_list
        self.recommand_check(self.evaluater_list, feature_list)

    def recommand_check(self, evaluater_list, feature_list):
        if self.options.all or self.options.mean_distance:
            print("mean_dis_alg")
            self.evaluater_mean_dis = self.single_recommend_simulation(feature_list, mean_dis_alg(not self.options.top1))
            evaluater_list.append(self.evaluater_mean_dis)
        if self.options.all or self.options.mean_pos_neg_dis:
            print("mean_pos_neg_dis_alg")
            self.evaluater_mean_pos_neg_dis = self.single_recommend_simulation(feature_list, mean_pos_neg_dis_alg(not self.options.top1))
            evaluater_list.append(self.evaluater_mean_pos_neg_dis)
        if self.options.all or self.options.min_distance:
            print("min_dis_alg")
            self.evaluater_min_dis = self.single_recommend_simulation(feature_list, min_dis_alg(not self.options.top1))
            evaluater_list.append(self.evaluater_min_dis)
        if self.options.all or self.options.map_to_low_dim:
            print("map_to_low_dim_alg")
            self.evaluater_map_to_low_dim = self.single_recommend_simulation(feature_list, map_to_low_dim_alg(not self.options.top1))
            evaluater_list.append(self.evaluater_map_to_low_dim)
        if self.options.all or self.options.min_pos_neg_dis:
            print("min_pos_neg_dis_alg")
            self.evaluater_min_pos_neg_dis = self.single_recommend_simulation(feature_list, min_pos_neg_dis_alg(not self.options.top1))
            evaluater_list.append(self.evaluater_min_pos_neg_dis)
        if self.options.all or self.options.knn != 0:
            print("knn_alg")
            self.evaluater_knn = self.single_recommend_simulation(feature_list, knn_alg(self.options.knn, not self.options.top1))
            evaluater_list.append(self.evaluater_knn)
         
    def compute_features(self, image_model):
        image_model.eval()
        with torch.no_grad(): 
            data = self.artigo_helper
            data.turn_off_data_augmentation()
            self.image_num = data.image_number
            loader = torch.utils.data.DataLoader(data, batch_size=96)
            res = []
            tag_matrix = []
            for (x_images,y_tag_matrix) in loader:  
                x_images, y_tag_matrix = x_images.to(device), y_tag_matrix.to(device) 
                image_features = image_model(x_images)
                res.append(image_features)
                tag_matrix.append(y_tag_matrix)
            self.tag_matrix = torch.cat(tag_matrix)
        return torch.cat(res)

    def check_features(self):
        image_model.eval()
        with torch.no_grad():
            features_2 = self.compute_features(self.image_model)
            length_1 = torch.sum(self.features * self.features, dim=1)
            print("min:", torch.min(length_1), ",  max:", torch.max(length_1))
            length_2 = torch.sum(self.features * features_2, dim=1)
            print("min:", torch.min(length_2), ",  max:", torch.max(length_2))
            length_3 = torch.sum(features_2 * features_2, dim=1)
            print("min:", torch.min(length_3), ",  max:", torch.max(length_3))

    def single_recommend_simulation(self, features, alg):
        
        with torch.no_grad():
            evaluater = artigo_evaluater(alg.name, self.tag_matrix, self.artigo_helper)
            evaluater.cluster_by_tags(['abstrakt', 'akt'])
            evaluater.cluster_by_tags(['bäume', 'landschaft'])
            evaluater.cluster_by_tags(['baum'])
            evaluater.cluster_by_tags(['mann', 'frau'])
            evaluater.cluster_by_tags(['kirche', 'foto'])
            evaluater.cluster_by_tags(['bäume', 'fluss', 'gras'])
            evaluater.cluster_by_tags(['meer', 'wasser', 'see', 'ufer', 'boot'])
            while evaluater.start_new_evaluation():
                print("evaluater.pos", evaluater.pos)
                first_index = evaluater.get_the_first_index()
                alg.set_first_element(features, first_index)
                display.display(display.Image(self.artigo_helper.get_image_path_by_index(first_index)))
                print("first_index",first_index)
                k=0
                while not evaluater.check_recommend_all_elements():
                    recommended_image_index = alg.get_recommendation()
                    res = evaluater.check_image_in_cluster(recommended_image_index.cpu().item())
                    if k < 5 :
                        k = k+1
                        display.display(display.Image(self.artigo_helper.get_image_path_by_index(recommended_image_index)))
                        print("recommended_image_index",recommended_image_index, res)
                    if res:
                        alg.update(recommended_image_index.cpu().item(), True)
                    else:
                        alg.update(recommended_image_index.cpu().item(), False)

        return evaluater

    def recommend_with_pos_neg(self, pos_features, neg_features, alg, n=5):
        
        with torch.no_grad():
            alg.set_first_element(self.features, pos_features[0])
            display.display(display.Image(self.artigo_helper.get_image_path_by_index(pos_features[0])))
            print("pos",pos_features[0])
            for i in range(1,len(pos_features)):
                display.display(display.Image(self.artigo_helper.get_image_path_by_index(pos_features[i])))
                print("pos",pos_features[i])
                alg.update(pos_features[i], True)
            for i in range(len(neg_features)):
                display.display(display.Image(self.artigo_helper.get_image_path_by_index(neg_features[i])))
                print("neg",neg_features[i])
                alg.update(neg_features[i], False)
            for k in range(n):
                recommended_image_index = alg.get_recommendation()
                display.display(display.Image(self.artigo_helper.get_image_path_by_index(recommended_image_index)))
                print("recommended_image_index",recommended_image_index)
                alg.set_visited(recommended_image_index)
        return 

    def compare(self, evaluater_a, evaluater_b):
        color = ["red", "blue"]
        for i in range(len(evaluater_a.cluster_list)):
            cluster_a = evaluater_a.cluster_list[i]
            cluster_b = evaluater_b.cluster_list[i]
            print("key-name", cluster_a.key," --- ", cluster_a.name)
            print("color:", color[0],", alg:", evaluater_a.name)
            print("color:", color[1],", alg:", evaluater_b.name)

            self.visualization(cluster_a.result_list, cluster_b.result_list, color)
        return
    
    def compute_mean_distance(self):
        with torch.no_grad():
            batch_size = 100
            dis = []
            p = 0
            while p < len(self.features):
                np = min(p+batch_size,len(self.features))
                dis.append(torch.mean(self.features[p:np] @ self.features.t(), dim=1))
                p = np
            dis = torch.cat(dis)
            print("mean", torch.mean(dis))
            return
    
    def print_score(self):
        self.evaluater_list[0].print_clusters()
        for evaluater in self.evaluater_list:
            name = evaluater.name + ''.join([' ']*(max(25-len(evaluater.name),0))) + 'score:'
            print("name:", name, evaluater.compute_score())

    def visualization(self, list_a, list_b, color=["red", "blue"]):
        f, ax = plt.subplots()
        ax.set_xbound((0,1.0))
        ax.set_ybound((0, self.image_num))

        patch = []
        for i in range(len(list_a)):
            patch = mpl.patches.Rectangle((0.1*i, 0), 0.1, list_a[i], facecolor=color[0], alpha=0.3)
            ax.add_patch(patch)
        for i in range(len(list_b)):
            patch = mpl.patches.Rectangle((0.1*i, 0), 0.1, list_b[i], facecolor=color[1], alpha=0.3)
            ax.add_patch(patch)
        plt.show()

    def print_table(self):
        print(self.compute_cluster_info_table())
        print(self.compute_evaluation_table())
        print(self.compute_ratio_table())
    
    def compute_cluster_info_table(self):
        image_num = self.evaluater_list[0].compute_image_num()
        key = self.evaluater_list[0].compute_key() 
        cluster = list(range(len(image_num)))
        tag_num_f10 = self.evaluater_list[0].compute_frequent_tag_number(frequency=1)
        tag_num_f5 = self.evaluater_list[0].compute_frequent_tag_number(frequency=0.5)
        tag_num_f3 = self.evaluater_list[0].compute_frequent_tag_number(frequency=0.3)
        mean_max_freq = self.evaluater_list[0].compute_mean_max_frequency()
        median_max_freq = self.evaluater_list[0].compute_median_max_frequency()
        mean_tag_num = self.evaluater_list[0].compute_mean_tag_num()
        appeared_tag_num = self.evaluater_list[0].compute_appeared_tag_num()
        self.cluster_info_table = pd.DataFrame({'cluster':cluster,
                            'key':key,
                            'image_num':image_num,
                            'appeared_tag_num':appeared_tag_num,
                            'mean_tag_num':mean_tag_num,
                            'mean_max_freq':mean_max_freq,
                            'median_max_freq':median_max_freq,
                            'tag_num_f10':tag_num_f10,
                            'tag_num_f5':tag_num_f5,
                            'tag_num_f3':tag_num_f3,}, 
                            columns=['key', 'image_num', 'appeared_tag_num', 'tag_num_f10', 'tag_num_f5', 'tag_num_f3',
                            'mean_tag_num', 'mean_max_freq', 'median_max_freq'])
        self.cluster_info_table.round(3)
        return self.cluster_info_table  

    def compute_evaluation_table(self):
        key = self.evaluater_list[0].compute_key()
        self.evaluation_table = pd.DataFrame({'key':key}, 
                            columns=['key'])
        for evaluater in self.evaluater_list:
            self.evaluation_table[evaluater.name] = evaluater.compute_score_list()
        self.evaluation_table.round(3)
        return self.evaluation_table

    def compute_ratio_table(self, second_evaluater_list=None):
        if second_evaluater_list == None:
            second_evaluater_list = self.basic_evaluater_list
        key = self.evaluater_list[0].compute_key()
        self.ratio_table = pd.DataFrame({'key':key}, 
                            columns=['key'])
        for i in range(len(self.evaluater_list)):
            score_list_1 = self.evaluater_list[i].compute_score_list()
            score_list_2 = second_evaluater_list[i].compute_score_list()
            temp_list = np.asarray(score_list_1) / np.asarray(score_list_2) 
            self.ratio_table['ratio'+self.evaluater_list[i].name] = temp_list
        self.ratio_table.round(3)
        return self.ratio_table 

    def compute_baseline(self):
        image_model = Image_Model('ResNet101').to(device) 
        
        # compute feature for all images
        feature_list = self.compute_features(image_model)
        self.recommand_check(self.basic_evaluater_list, feature_list)

   
if __name__ == "__main__":
    recommender().recommend()