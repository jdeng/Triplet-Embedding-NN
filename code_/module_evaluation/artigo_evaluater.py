from concurrent.futures import thread
import random
import pandas as pd
from threading import Thread
import shutil
import os
import re
import torch
import numpy as np
from torch.distributions import Categorical, kl

Image_Path = './dataset/artigo/images/'
dataset_Path = './dataset/artigo/'
Image_Tags_Path = './dataset/artigo/image_tags.csv'

Cluster_Path = './cluster/'

if torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

IMAGE_HEIGHT = 224
IMAGE_WIDTH = 224
IMAGE_CHANNEL = 3

class artigo_evaluater ():

    class my_cluster ():
        def __init__(self, key, name, tag_matrix):
            # "artist_id" or "title"
            self.key = key
            self.name = name
            self.tag_matrix = tag_matrix
            #print("key-name",key, name)
            # 10% - 100%
            self.mAP_sum = 0
            self.correct_num = 0
            self.recommendation_times = 0
        
        def compute_index_set_based_on_tags (self, tag_vector, data, save_image=False):
            data['tag'] = (self.tag_matrix.float() @ tag_vector.to(device).t()).squeeze().cpu().tolist()
            tags = self.name
            self.name = torch.sum(tag_vector).item()
            self.compute_index_set (data, save_image=save_image)
            self.name = tags
            return
        
        def compute_index_set (self, data, save_image=False):
            data = data[["index", self.key, "path"]]
            data = data[data[self.key] == self.name]
            if save_image:
                self.save_images(data["path"])
            self.data = data[["index", "path"]]
            self.image_state = dict(zip(data["index"].tolist(), [-1]*len(data)))
            self.amount = len(data)
            # the first
            random.seed(10)
            row = random.randint(0, self.amount-1)
            self.first_image_index = self.data.iloc[row]["index"]
            #print("self.first_image_index ", self.first_image_index )
            self.check_image_in_cluster(self.first_image_index)

        def check_image_in_cluster (self, image_index):
            self.recommendation_times = self.recommendation_times + 1
            is_in_cluster = True
            #print("recommendation_times", self.recommendation_times, "correct_num", self.correct_num, "image_index",image_index)
            if image_index not in self.image_state:
                is_in_cluster = False
            elif self.image_state[image_index] == -1:
                self.image_state[image_index] = 1
                self.correct_num = self.correct_num + 1
                self.mAP_sum = self.mAP_sum +  self.correct_num / self.recommendation_times
            else:
                print("repeating picture")
            return is_in_cluster

        def save_images(self, path_list):
            directory = Cluster_Path + self.key + "/" + str(self.name).replace(" ", "_")[0:min(10,len(str(self.name)))]
            if not os.path.exists(directory):
                os.makedirs(directory)
            for path in path_list:
                p_read = Image_Path + path
                p_write = directory + "/" + path
                try:
                    shutil.copy(p_read, p_write)
                except IOError as e:
                    print("Unable to copy file. %s" % e)
            return

        def check_recommend_all_elements (self):
            if self.correct_num == self.amount:
                self.compute_tag_set_parameters()
                return True
            else:
                return False   

        def print_result(self, total_num):
            print("key-name",self.key, self.name)
            print("num in cluster: ", len(self.data), "   , total number: ", total_num)
            print(self.mAP_sum/self.correct_num)

        def compute_frequent_tag_number(self, frequency):
            return torch.sum((self.tag_sum / (frequency * self.amount)).int()).item()

        def compute_tag_set_parameters(self):

            tag_set = []
            for index in self.data["index"].tolist():
                #print(tag_matrix[id_index_dict[id],:])
                tag_set.append(self.tag_matrix[index,:])
            tag_set = torch.stack(tag_set)
            self.tag_sum = torch.sum(tag_set, dim=0)

            tag_frequency = self.tag_sum / self.amount
            self.mean_max_frequency = torch.mean(torch.max(tag_set * tag_frequency.view(1, -1), dim=1)[0]).item()
            self.median_max_frequency = torch.median(torch.max(tag_set * tag_frequency.view(1, -1), dim=1)[0]).item()

            self.mean_tag_num = (torch.sum(self.tag_sum) / self.amount).item()

            self.appeared_tag_num = len(self.tag_sum.nonzero())

            return 

            #print(tag_sum)
            half = torch.tensor(self.amount/2).to(device)
            distribution = (half - torch.abs(tag_sum-half)) / half
            self.distribution = distribution
            #print("distribution ", distribution.size(), "sum ", torch.sum(distribution))
            self.distribution_sum = torch.sum(distribution).item()
            probabilities = distribution / torch.sum(distribution)
            #print("probabilities ", probabilities.size(), "sum ", torch.sum(probabilities))
            self.entropy = Categorical(distribution).entropy().item()
        

    def __init__(self, name, tag_matrix, artigo_helper=None):
        self.name = name
        self.cluster_list = []
        self.tag_matrix = tag_matrix
        self.pos = -1
        self.artigo_helper = artigo_helper

        # read image_list
        "id","artist_id","title","not_before","not_after","location","institution","path"
        self.image_list = pd.read_csv(dataset_Path + "resource.csv")  
        self.image_list["title"] = self.image_list["title"].map(self.delete_brackets)  
        self.image_list["index"] = list(range(len(self.image_list)))
        self.cluster_by_key("artist_id", 260)
        self.cluster_by_key("title", 80)

    def cluster_by_tags(self, tag_set):
        self.cluster_list.append(self.my_cluster("tag", tag_set, self.tag_matrix))
        tag_vector = self.artigo_helper.get_tag_matrix_based_on_tagset(tag_set)
        self.cluster_list[-1].compute_index_set_based_on_tags(tag_vector, self.image_list, save_image=False)
        return

    def cluster_by_key(self, key, min_fre=100):
        frequent_items = self.image_list[key].value_counts()
        frequent_items = frequent_items[frequent_items >= min_fre]
        #print("len of frequent_items"":", len(frequent_items))
        '''
        for item in frequent_items.index:
            self.cluster_list.append(self.my_cluster(key, item))
            self.cluster_list[-1].compute_index_set(self.image_list, True)
        return 
        '''
        thread_list = []
        for item in frequent_items.index:
            self.cluster_list.append(self.my_cluster(key, item, self.tag_matrix))
            t = Thread(target=self.cluster_list[-1].compute_index_set, args=[self.image_list, True])
            t.start()
            thread_list.append(t)
        for t in thread_list:
            t.join()
        return

    def delete_brackets (self, s):
        if type(s) == str:
            a = r'\(.*?\)'
            ext = re.sub(a, '', s)
            return ext
        return s

    # choose a new cluster to evalue recommender system
    def start_new_evaluation(self, num=1):
        if self.pos != -1:
            self.cluster_list[self.pos].print_result(len(self.image_list))
        self.pos = self.pos + 1
        if self.pos < len(self.cluster_list):
            return True
        return False

    def get_the_first_index(self):
        return self.cluster_list[self.pos].first_image_index

    # receive a recommended image and return whether it belongs to the cluster
    # record the number of calls, and output the result

    def check_image_in_cluster(self, image_index):
        return self.cluster_list[self.pos].check_image_in_cluster(image_index)   

    # return whether the recommender system has recommended all images in the cluster
    def check_recommend_all_elements (self):
        return self.cluster_list[self.pos].check_recommend_all_elements()

    def compute_score(self):
        self.compute_score_list()
        return sum(self.score_list) / len(self.score_list)

    def compute_score_list(self):
        self.score_list = []
        for c in self.cluster_list:
            self.score_list.append(c.mAP_sum/c.correct_num)
        return self.score_list

    def compute_mean_tag_num(self):
        self.mean_tag_num_list = []
        for c in self.cluster_list:
            self.mean_tag_num_list.append(c.mean_tag_num)
        return self.mean_tag_num_list

    def compute_appeared_tag_num(self):
        self.appeared_tag_num_list = []
        my_list = self.appeared_tag_num_list
        if len(my_list) == 0:
            for c in self.cluster_list:
                my_list.append(c.appeared_tag_num)
        return my_list

    def compute_frequent_tag_number(self, frequency=0.5):
        self.frequent_tag_number_list = []
        my_list = self.frequent_tag_number_list
        if len(my_list) == 0:
            for c in self.cluster_list:
                my_list.append(c.compute_frequent_tag_number(frequency))
        return my_list

    def compute_mean_max_frequency(self):
        self.mean_max_frequency_list = []
        my_list = self.mean_max_frequency_list
        if len(my_list) == 0:
            for c in self.cluster_list:
                my_list.append(c.mean_max_frequency)
        return my_list

    def compute_median_max_frequency(self):
        self.median_max_frequency_list = []
        my_list = self.median_max_frequency_list
        if len(my_list) == 0:
            for c in self.cluster_list:
                my_list.append(c.median_max_frequency)
        return my_list
 
    def compute_image_num(self):
        self.image_num_list = []
        if len(self.image_num_list) == 0:
            for c in self.cluster_list:
                image_num = c.amount
                self.image_num_list.append(image_num)
        return self.image_num_list 

    def compute_key(self):
        self.key_list = []
        if len(self.key_list) == 0:
            for c in self.cluster_list:
                key = c.key
                self.key_list.append(key)
        return self.key_list 

    def print_clusters(self):
        for c in self.cluster_list:
            print("key-name",c.key, c.name)
        return
    